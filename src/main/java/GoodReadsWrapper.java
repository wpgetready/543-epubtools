import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class provides a wrapper for the GoodReads API
 */

public class GoodReadsWrapper {
    static String apiKey ="rkZqJsUs8UIJ7ZSNMI0aJQ"; //api needed. I registered to GoodReads for that
    static String urlAuthor ="https://www.goodreads.com/author/show.xml?key=%s&id=%s"; //id is the id author in GoodReads Author's database.



    //This method regenerates data from GoodsReads api site
    public static AuthorData getAuthorData(int authorId)  {
        try {
            String u =String.format(urlAuthor,apiKey,authorId);
            String xmlResponse = getHTML(u);
            return fillAuthor(xmlResponse);
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        }

        return null;
    }

    //This method parses author data from GoodReads, returning a proper AuthorData object, ready to be used for cretaing biography page
    private static AuthorData fillAuthor (String xmlData) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xmlData));
        Document doc = builder.parse(is);
        //traverse de document to get the information
        AuthorData ad = new AuthorData();
        Element root = doc.getDocumentElement();
        NodeList authors =root.getElementsByTagName("author");
        Element author = (Element) authors.item(0);

        ad.name = author.getElementsByTagName("name").item(0).getTextContent();
        ad.biography = author.getElementsByTagName("about").item(0).getTextContent();
        ad.bornDate = author.getElementsByTagName("born_at").item(0).getTextContent();
        ad.deathDate = author.getElementsByTagName("died_at").item(0).getTextContent();
        ad.urlImage = author.getElementsByTagName("large_image_url").item(0).getTextContent();
        //Now, loop over all the books this author has
        Element rootBooks = (Element) author.getElementsByTagName("books").item(0);
        NodeList books = rootBooks.getElementsByTagName("book");

        Element currentElement;
        for (int i = 0; i < books.getLength(); i++) {
            BookData bd = new BookData();
            currentElement = (Element)books.item(i);
            bd.title =currentElement.getElementsByTagName("title").item(0).getTextContent();
            bd.googlePlayUrl =""; //Only if this book was published, get it from metadata.
            bd.url = currentElement.getElementsByTagName("link").item(0).getTextContent();
            bd.urlImage = currentElement.getElementsByTagName("large_image_url").item(0).getTextContent();
            //If there is no large image, set the smal image
            bd.imageIsSmall=false;
            if (bd.urlImage.trim().length()==0) {
                bd.urlImage = currentElement.getElementsByTagName("small_image_url").item(0).getTextContent();
                bd.imageIsSmall=true;
            }
            ad.books.add(bd);
        }
        return ad;
    }


    //20181224: A stupid omition: please send the charset ,otherwise data is corrupted...
    private static String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }




}
/*Basic structure for example:

<GoodreadsResponse>
    <Request>
        <authentication>true</authentication>
        <key><![CDATA[rkZqJsUs8UIJ7ZSNMI0aJQ]]></key>
        <method><![CDATA[author_show]]></method>
    </Request>
    <author>
        <id>5217</id>
        <name>George Bernard Shaw</name>
        <link>
            <![CDATA[https://www.goodreads.com/author/show/5217.George_Bernard_Shaw]]>
        </link>
        <fans_count type="integer">2608</fans_count>
        <author_followers_count type="integer">2608</author_followers_count>
        <large_image_url>
            <![CDATA[https://images.gr-assets.com/authors/1271683549p7/5217.jpg]]>
        </large_image_url>
        <image_url>
            <![CDATA[https://images.gr-assets.com/authors/1271683549p5/5217.jpg]]>
        </image_url>
        <small_image_url>
            <![CDATA[https://images.gr-assets.com/authors/1271683549p2/5217.jpg]]>
        </small_image_url>
        <about>
            <![CDATA[George Bernard Shaw was an Irish playwright, socialist, and a co-founder of the London School of Economics. Although his first profitable writing was music and literary criticism, in which capacity he wrote many highly articulate pieces of journalism, his main talent was for drama. Over the course of his life he wrote more than 60 plays. Nearly all his plays address prevailing social problems, but each also includes a vein of comedy that makes their stark themes more palatable. In these works Shaw examined education, marriage, religion, government, health care, and class privilege.<br /><br />An ardent socialist, Shaw was angered by what he perceived to be the exploitation of the working class. He wrote many brochures and speeches for the Fabian Society. He became an accomplished orator in the furtherance of its causes, which included gaining equal rights for men and women, alleviating abuses of the working class, rescinding private ownership of productive land, and promoting healthy lifestyles. For a short time he was active in local politics, serving on the London County Council.<br /><br />In 1898, Shaw married Charlotte Payne-Townshend, a fellow Fabian, whom he survived. They settled in Ayot St. Lawrence in a house now called Shaw's Corner. <br /><br />He is the only person to have been awarded both a Nobel Prize for Literature (1925) and an Oscar (1938). The former for his contributions to literature and the latter for his work on the film "Pygmalion" (adaptation of his play of the same name). Shaw wanted to refuse his Nobel Prize outright, as he had no desire for public honours, but he accepted it at his wife's behest. She considered it a tribute to Ireland. He did reject the monetary award, requesting it be used to finance translation of Swedish books to English.<br /><br />Shaw died at Shaw's Corner, aged 94, from chronic health problems exacerbated by injuries incurred by falling.]]>
        </about>
        <influences>
            <![CDATA[<a href="https://www.goodreads.com/author/show/156323.Robert_Ingersoll" title="Robert Ingersoll" rel="nofollow">Robert Ingersoll</a>,<a href="https://www.goodreads.com/author/show/194905.Avro_Manhattan" title="Avro Manhattan" rel="nofollow">Avro Manhattan</a>,<a href="https://www.goodreads.com/author/show/211702.Annie_Besant" title="Annie Besant" rel="nofollow">Annie Besant</a>,<a href="https://www.goodreads.com/author/show/11682.Arthur_Schopenhauer" title="Arthur Schopenhauer" rel="nofollow">Arthur Schopenhauer</a>,<a href="https://www.goodreads.com/author/show/13890.Richard_Wagner" title="Richard Wagner" rel="nofollow">Richard Wagner</a>,<a href="https://www.goodreads.com/author/show/2730977.Henrik_Ibsen" title="Henrik Ibsen" rel="nofollow">Henrik Ibsen</a>,<a href="https://www.goodreads.com/author/show/1938.Friedrich_Nietzsche" title="Friedrich Nietzsche" rel="nofollow">Friedrich Nietzsche</a>,<a href="https://www.goodreads.com/author/show/162321.Henry_George" title="Henry George" rel="nofollow">Henry George</a>]]>
        </influences>
        <works_count>583</works_count>
        <gender>male</gender>
        <hometown>Dublin</hometown>
        <born_at>1856/07/26</born_at>
        <died_at>1950/11/02</died_at>
        <goodreads_author></goodreads_author>
        <books>
            <book>
                <id type="integer">7714</id>
                <isbn>0486282228</isbn>
                <isbn13>9780486282220</isbn13>
                <text_reviews_count type="integer">1422</text_reviews_count>
                <uri>kca://book/amzn1.gr.book.v1.KZWpyzc7mznHay_Qst0jiA</uri>
                <title>Pygmalion</title>
                <title_without_series>Pygmalion</title_without_series>
                <image_url>https://images.gr-assets.com/books/1453757285m/7714.jpg</image_url>
                <small_image_url>https://images.gr-assets.com/books/1453757285s/7714.jpg</small_image_url>
                <large_image_url/>
                <link>https://www.goodreads.com/book/show/7714.Pygmalion</link>
                <num_pages>82</num_pages>
                <format>Paperback</format>
                <edition_information/>
                <publisher>Dover Publications</publisher>
                <publication_day>20</publication_day>
                <publication_year>1994</publication_year>
                <publication_month>10</publication_month>
                <average_rating>3.90</average_rating>
                <ratings_count>80062</ratings_count>
                <description>One of George Bernard Shaw's best-known plays, Pygmalion was a rousing success on the London and New York stages, an entertaining motion picture and a great hit with its musical version, My Fair Lady. An updated and considerably revised version of the ancient Greek legend of Pygmalion and Galatea, the 20th-century story pokes fun at the antiquated British class system. &lt;br /&gt;&lt;br /&gt;In Shaw's clever adaptation, Professor Henry Higgins, a linguistic expert, takes on a bet that he can transform an awkward cockney flower seller into a refined young lady simply by polishing her manners and changing the way she speaks. In the process of convincing society that his creation is a mysterious royal figure, the Professor also falls in love with his elegant handiwork.&lt;br /&gt;&lt;br /&gt;The irresistible theme of the emerging butterfly, together with Shaw's brilliant dialogue and splendid skills as a playwright, have made Pygmalion one of the most popular comedies in the English language. A staple of college drama courses, it is still widely performed.</description>
                <authors>
                    <author>
                        <id>5217</id>
                        <name>George Bernard Shaw</name>
                        <role></role>
                        <image_url nophoto='false'>
                            <![CDATA[https://images.gr-assets.com/authors/1271683549p5/5217.jpg]]>
                        </image_url>
                        <small_image_url nophoto='false'>
                            <![CDATA[https://images.gr-assets.com/authors/1271683549p2/5217.jpg]]>
                        </small_image_url>
                        <link>
                            <![CDATA[https://www.goodreads.com/author/show/5217.George_Bernard_Shaw]]>
                        </link>
                        <average_rating>3.91</average_rating>
                        <ratings_count>153895</ratings_count>
                        <text_reviews_count>4465</text_reviews_count>
                    </author>
                </authors>
                <published>1994</published>
                <work>
                    <id>184399</id>
                    <uri>kca://work/amzn1.gr.work.v1.9jmyKyc3Lp0FfKT_vqjlaw</uri>
                </work>
            </book>
            ...(book structure continues)...
 */