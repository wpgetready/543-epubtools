import java.io.File;
import java.util.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.*;
import javax.xml.parsers.*;

/*

1-Cuando se agrega una página, esta se agrega al content.opf dentro del tag manifest:
<manifest>
    <item id="cover.xhtml" href="Text/cover.xhtml" media-type="application/xhtml+xml"/>
Si se agrega un estilo: <item id="page-css" href="Styles/page.css" media-type="text/css"/>
Si se agrega una imagen: <item id="logo-feedbooks-tiny" href="Images/logo-feedbooks-tiny.png" media-type="image/png"/>
o bien <item id="ebook-cover.jpg" href="Images/ebook-cover.jpg" media-type="image/jpeg"/>
 */
public class XmlTools {
    public static void main(String[] args) throws Exception {
    addFileToContentOPF( "Text" , "title.xhtml" );
    }


    public static void addFileToContentOPF(String folder, String filename ) throws Exception {

        String xmlPath = EpubTools.TMP_FOLDER+EpubTools.EPUB_FOLDER + "content.opf";
        File checkFile = new File(xmlPath);
        if (!checkFile.isFile()) {
            System.out.println("addFileToContentOPF  ERROR: missing file " + checkFile.toPath().toString());
            System.out.println("WARNING: This epub proably doesn't have standard folder structure....");
            return;
        }
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = null;
        try {
            document = documentBuilder.parse(xmlPath);
        } catch (Exception e)
        {
           System.out.println("Error:" + e.getMessage());
           return;
        }

        Element root =  document.getDocumentElement(); //Obtener el nodo raíz
        NodeList manifest = root.getElementsByTagName("manifest"); //Obtener el manifiesto.
        if (manifest== null) {
            System.out.println("addFileToContentOPF ERROR: I can't find manifest node in content.opf document");
            return;
        }

        if (manifest.getLength() !=1) {
            System.out.println("addFileToContentOPF ERROR: It should be just one manifest, but (" + manifest.getLength() + ") were found. Aborting...");
            return;
        }

        Node mani = manifest.item(0);

        String mediaType= getMediaType(filename);
        //First step: check if this node already exists (LATER for now)

        Element  item = document.createElement("item");
        item.setAttribute("id",filename); //currently id is the filename too
        item.setAttribute( "href", folder + "/" + filename);
        item.setAttribute("media-type", mediaType);

        mani.appendChild(item);

        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult(xmlPath);
        transformer.transform(source, result);
    }

    /**
     * Get media type ,UNDEFINED if the file has some unknown extension.
     * @param filename
     * @return
     */
    public static String getMediaType(String filename) {
        String extension = FilenameUtils.getExtension(filename);
        //For some reason switch function refuses to work properly
        if (extension.endsWith("xhtml")){return "application/xhtml+xml";        }
        if (extension.endsWith("html")) {return "application/html";        }
        if (extension.endsWith("htm"))  {return "application/html";        }
        if (extension.endsWith("xml"))  {return "application/xml";        }
        if (extension.endsWith("css"))  {return "text/css";        }
        if (extension.endsWith("png"))  {return "image/png";        }
        if (extension.endsWith("jpg"))  {return "image/jpeg";        }
        return "UNDEFINED";
    }

    public static class Server {
        public String getName() { return "foo"; }
        public Integer getPort() { return 12345; }
    }
}