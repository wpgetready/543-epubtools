public class BookData {
    public String title;
    public String url;
    public String urlImage;
    public boolean imageIsSmall;
    public String googlePlayUrl;
}