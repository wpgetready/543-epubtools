import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipTools {

    public static final String testDir = "c:/ESPACIO-TRABAJO/IntelliJIdea/543-EpubTools/temp/epub/";
    public static final String IGNORE_INITIAL_FOLDER = "FZSTART"; //Trick to ignore initial folder , in this exaple
    public static final String testZipFile = "c:/ESPACIO-TRABAJO/IntelliJIdea/archive.zip";
    //I will ignore 'epub' , and only compress inside.

    /***
     * Use this method for testing purposes
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        zipDir(testDir,testZipFile,true);
    }

    public static void zipDir (String dirToCompress,String zipFilename) throws Exception {
        zipDir(dirToCompress,zipFilename,true);
    }

    /***
     * Compress a directory inside a zip file
     * Specify Directory, filename and decide if you need compress the foldername or just the files inside of it.
     * @param dirToCompress
     * @param zipFilename
     * @param ignoreInitialFolder
     * @throws Exception
     */
    public static void zipDir (String dirToCompress,String zipFilename,boolean ignoreInitialFolder) throws Exception {
        FileOutputStream fos = new FileOutputStream(zipFilename);
        ZipOutputStream zos = new ZipOutputStream(fos);
        String IgnoreFolder = (ignoreInitialFolder)?IGNORE_INITIAL_FOLDER:null;
        addDirToZipArchive(zos, new File(dirToCompress), IgnoreFolder);
        zos.flush(); fos.flush();
        zos.close(); fos.close();
    }

    public static void addDirToZipArchive(ZipOutputStream zos, File fileToZip, String parentDir) throws Exception {
        if (fileToZip == null || !fileToZip.exists()) {
            System.out.println("addDirToZipArchive: file to zip null or not exists:" + fileToZip + " .Aborted!!!");
            return;
        }

        String zipEntryName = fileToZip.getName();
        if (parentDir!=null && !parentDir.isEmpty()) {
            if(parentDir.equals("FZSTART")) {
                zipEntryName =  "" ; //ignore epub folder
            } else
            {
                zipEntryName = parentDir + "/" + fileToZip.getName();
            }

        }

        if (fileToZip.isDirectory()) {
           // System.out.println("+" + zipEntryName);
            for (File file : fileToZip.listFiles()) {
                addDirToZipArchive(zos, file, zipEntryName);
            }
        } else {
            //System.out.println("   " + zipEntryName);
            byte[] buffer = new byte[1024];
            FileInputStream fis = new FileInputStream(fileToZip);
            zos.putNextEntry(new ZipEntry(zipEntryName));
            int length;
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            zos.closeEntry();
            fis.close();
        }
    }

    public static void unZipIt(String zipFilePath,String outputFolder) {

        try {
            // Open the zip file
            File folder = new File(outputFolder);             //create output directory is not exists
            if(!folder.exists()){ folder.mkdir();  }

            ZipFile zipFile = new ZipFile(zipFilePath);
            Enumeration<?> enu = zipFile.entries();
            while (enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) enu.nextElement();

                String name = zipEntry.getName();
                long size = zipEntry.getSize();
                long compressedSize = zipEntry.getCompressedSize();
               // System.out.printf("name: %-20s | size: %6d | compressed size: %6d\n", name, size, compressedSize);

                // Do we need to create a directory ?
                File file = new File(outputFolder + File.separator + name);
                if (name.endsWith("/")) {
                    file.mkdirs();
                    continue;
                }

                File parent = file.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }

                // Extract the file
                InputStream is = zipFile.getInputStream(zipEntry);
                FileOutputStream fos = new FileOutputStream(file);
                byte[] bytes = new byte[1024];
                int length;
                while ((length = is.read(bytes)) >= 0) {
                    fos.write(bytes, 0, length);
                }
                is.close();
                fos.close();

            }
            zipFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}