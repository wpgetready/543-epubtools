import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.*;

import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/*
General purpose utilities to simplify creating content from epub and using Libribox to integrate audio to epubs.
20180824: Improved: mp3 extraction separates mp3 from description
          Improved: epub formatting uses insert_audio_after tag for matching and configuirng per ebook.
20180827: Improved: generic scripts to automate compilation from ebook metadata. Currently in beta.
20180827: Improved: passing parameteers through command line is now accepted(!)
989 lines including comments. Trying to simplificate a bit...
20180829: Including documentation 1014 lines...
Growing and growing: rizar el rizo. Le agrego una clase para dezipear los archivos epub y realizar el proceso
de extracción completo. Solo lleva unos pocos segundos, pero he verificado que suelo equivocarme a menudo.
20180907: Rizando el rizo: chequeo ADEMAS que las dimensiones de las imagenes son las esperadas, algo que me sucede a menudo
y es preciso evitar.
20180910: corregido: cuando se descomprime y se generar mp3.json / toc.json si se sobreescribe, se borra antes los archivos en cuestión.
20180910: inicia de la opción repackaging. Es una boludez, pero es preferible tenerlo hecho.
20180910: primeros test de la opción repackaging. Como suele pasar, al ver el proceso completo, me sorprende la cantidad de partes que
SI se pueden automatizar y que llevo de manera subconciente...
20180911: Añado IMDB page generator
20180912: Añado ASO generator, y pruebo ambos scripts, andan bien.
20180914:
20180914: Añado default options, de manera de decir que si o no a las respuestas, para automatizar el proceso un poco más.
20180914: Añadida en el metadata la variable audio-regexp que me permite determinar la expresion regular de captura de descripcion
de los audios mp3, anteriormente solo hardcoded.
20180914: Proyectado a futuro proximo:
-consolidar metadata de IMDB y ASO en el archivo de metadata.
-Lectura de arrays desde el metadata usando !variable....!END
-Inyectar las páginas faltantes a la hora de extraer el ebook. (title.xhtm (que debe generarse por template)
 important.xhtml, imdb.xhtml y theend.xhtml
 20180918: Se inyectan automáticamente las páginas que son necesarias para el libro: important.xhtml, imdb.xhtml, theend.xhtml, title.xhtml
 y cover.xhtml (a definir esta última). Además se agregan las imagenes asociadas.
 Por lo tanto, el PRIMER paso que se hace de un epub es un extract y un repackage, de esta manera no hay que agregar manualmente las páginas.
 Luego se debería ordenar el toc acorde y volver a repetir el procedimiento para unir el audio.
20180918: se inyecta cover.xhtml. Creo un script para generar el cover , los iconos de 512x512 y el icono 500x1024 automaticamente.
Me falta el icono circular de 512x512. Con esto, la generación de los ebooks se automatiza varios pasos mas, empujando hacia adelante
la intervención manual (que pasa a ser opcional, pero recomendada desde luego)
 20181126: Agregado opcion para crear carpetas y para bajar archivos directamente de Gutenberg.
 Combinando ambos es posible escalar y bajar n ebooks y dejarlos prontos para su analisis.
20181204:Encuentro una manera de generar las biografias casi automáticamente, reduciendo los tiempos
más que drásticamente: verifico que a mano genero apenas 5 biografías por hora.
Veamos que velocidad tiene esta api...
20190217: Despues de varios días de trabajo, comienzo a aproximarme a lo que será la siguiente versión de FolioReader.
Migré el container completo a la última versión de FolioReader. Llevó mucho trabajo, porque el que desarrolló parece
que juega con la tecnología: en 6 meses paso de Android 2.x a 3.x migro gran cantidad de cosas a Kotlin, cambió
los layouts de Relative a Constraint, refactorizá gran cantidad de clases y código , cambió los gradles de los samples
cambio strings.xml y le agregó multilenguaje...
Prácticamente no dejó cosa sin cambiar, lo que habla mucho tambien lo inestable y cambiante que es este proyecto.
Aunque afortunadamente no fue demasiado traumático generó muchos inconvenientes y tuve que hacer algunas readaptaciones al proyecto
Recordemos que el proyecto además usa ciertos templates basado en FolioReader los cuales tuve que cambiar, porque EpubTools
REESCRIBE strings.xml y build.gradle, algo que me di cuenta tarde cuando ya había hecho el destrozo.
strings.xml le hice un merge lo cual permite hacer un switch del proyecto 541-FolioReader al 549-FolioReaderPlus sin incidentes.
Sin embargo, build.gradle es otra cosa completamente diferente, por lo que existe un build.gradle.541 para hacerlo compatible.
Esto tengo que seguir inspeccionandolo en el TEORICO caso de que fuera hacia atrás, cosa que no veo que haga por lo pronto.
20190219: Rollback del proyecto hacia la versión anterior de FolioReader.
Despues de un análisis de los resultados y el encontrar el proyecto Librerar, decido hacer marcha atrás con los cambios (espero haber tenido exito)
Las razones son:
-La nueva versión de FolioReader tiene muchísimos cambios y provee UN VALOR AGREGADO MINIMO O NULO. Solamente migro para agregar busqueda?
-Inmensa cantidad de cambios que implicó un montó de trabajo: Conversion de clases Java a Kotlin(?) cambios de layout, cambios de código etc.
-La versión actual CONTIENE MUCHOS ERRORES Y NO ES ESTABLE.
Migrar tiene que tener una razón FUERTE: tiene que proporcionar VALOR AGREGADO. Vengo hace 3 días migrando y además de generar un montón de problemas
fue una migración que logré, pero tiene DEMASIADOS ERRORES que no son de mi autoría. Y luego de ver como funciona Librera, hago un paso atrás definitivo.
 */

public class EpubTools {


    //Link for downloading ebooks,wathcout the ebooks folder!!!
    public static final String GUTENBERG_SITE_URL = "http://www.gutenberg.org/ebooks/";

    public enum validationLevel { UNPACKAGE,REPACKAGE,COMPILE,GOOGLE_PLAY,ALL}


    public static final String TMP_FOLDER = "C:/ESPACIO-TRABAJO/IntelliJIdea/543-EpubTools/temp/";
    public static final String EPUB_FOLDER= "epub/OEBPS/"; //location epub pages inside epub uncompresed package
    public static final String REGEXP_INSERT_AUDIO = "<\\/h2>"; //Place where we'll insert audio fragment
    public static final String REGEXP_BODY_TAG ="<body(.*?)>"; //Tag to match search
    public static final String BODY_TAG_RESET = "<body>"; //Tag to reset any type of search found.
    public static final String BODY_TAG_SCRIPT_TEMPLATE = "<body onload=\"document.getElementById('audiomp3').currentTime = %s;\">";
    public static final String REGEXP_FIND_MP3_64KB = "(http://\\S+64kb\\.mp3)\"";
    public static final String REGEXP_FIND_MP3_TITLE = "(http://\\S+128kb\\.mp3)(.*?)>(.*?)<"; //No matther what the THIRD group is the important one.

    //Place where all ebooks template and metadata are found.
    public static final String ALEXANDRIA_ROOT = "c:/ESPACIO-TRABAJO/ALEXANDRIABOOKSENG-METADATA/";
    public static final String EBOOK_ICONS = ALEXANDRIA_ROOT + "%s" + "/graphics/icons/";
    public static final String EBOOK_SCREENSHOTS = ALEXANDRIA_ROOT + "%s" + "/graphics/screenshots/";
    public static final String EBOOK_COVER = ALEXANDRIA_ROOT + "%s" + "/graphics/cover/";
    public static final String EBOOK_KEYSTORE= ALEXANDRIA_ROOT + "%s" + "/keystore/MAIN-KEYSTORE.keystore";
    public static final String EBOOK_EPUB = ALEXANDRIA_ROOT + "%s" + "/epub/";
    public static final String EBOOK_APK = ALEXANDRIA_ROOT + "%s" + "/apk/";
    public static final String EBOOK_OTHER = ALEXANDRIA_ROOT + "%s" + "/other/";
    public static final String EBOOK_EVERYTHING = ALEXANDRIA_ROOT + "%s" + "/$everything/";
    public static final String EBOOK_AUTHORS_ROOT = ALEXANDRIA_ROOT + "authors/";
    public static final String EBOOK_AUTHORS = ALEXANDRIA_ROOT + "authors/%s/";
    public static final String EBOOK_CUSTOM_PAGE = ALEXANDRIA_ROOT + "ebook000/OEBPS/";
    public static final String EBOOK_COMMON_SCREENSHOTS = ALEXANDRIA_ROOT + "ebook000/common-screenshots/";
    public static final String EBOOK_TRASH_FOLDER = ALEXANDRIA_ROOT + "ebook000/trash/";
    public static final String EBOOK_FOLDER_STRUCTURE = ALEXANDRIA_ROOT + "ebook000/ebook-structure/";
    public static final String AUTHOR_FOLDER_STRUCTURE = ALEXANDRIA_ROOT + "ebook000/author-structure/";

    //Temp file where script works to perform merging audio with content
    public static final String MP3_JSON_PATH =  TMP_FOLDER + "mp3.json";
    public static final String TOC_JSON_PATH = TMP_FOLDER + "toc.json";

    //The following data SHOULD BE EXTRACTED FROM EBOOK METADATA(!)
    //public static final String METADATA_MP3_URL = "https://librivox.org/fairy-tales-by-the-brothers-grimm/";
    public static final String METADATA_NOT_FOUND = "<NOT FOUND>";
    public static final String METADATA_EMPTY = "<EMPTY>";

    //The following data points to project FolioReader
    public static final String FOLIO_READER_ROOT = "C:/ESPACIO-TRABAJO/AndroidStudio/541-FolioReader/code/";
    //public static final String FOLIO_READER_ROOT = "C:/ESPACIO-TRABAJO/AndroidStudio/549-FolioReaderPlus/code/";
    //Two basic files needed to be changed in order of being according with epub's metadata.
    public static final String FR_BUILD_GRADLE_FILE = FOLIO_READER_ROOT + "sample/build.gradle";
    public static final String FR_STRINGS_XML = FOLIO_READER_ROOT + "folioreader/res/values/strings.xml";
    //Place epub needs to be copied in order of starting the compilation
    public static final String FR_EPUB_SOURCE = FOLIO_READER_ROOT + "sample/src/main/assets/";
    //Place where the icons need to be copied, including directories
    public static final String FR_EPUB_ICONS = FOLIO_READER_ROOT + "sample/src/main/res/";
    //Place where Android Stuido compiles the final apk
    public static final String FR_APK_RELEASE = FOLIO_READER_ROOT + "sample/build/outputs/apk/release/sample-release.apk";
    public static final int IMDB_IMG_WIDTH = 336;
    public static final int IMDB_IMG_HEIGHT = 475;

    public static String defaultAnswer=""; //This is used to avoid repetitive answer to simple questions

    public static void main(String[] args) throws Exception
    {
        /* Parsers everywhere
        https://stackoverflow.com/questions/367706/how-do-i-parse-command-line-arguments-in-java
        I'm going to use commons-cli , but always KISS: Keep It Simple,S....
        Passing parameters to the command line : https://stackoverflow.com/questions/2066307/how-do-you-input-commandline-argument-in-intellij-idea
        In Short: Run->Edit configurations->Program arguments
         */

        /*
        GoodReadsWrapper.getAuthorData(5217);
        if (true) return;
*/
        Options options = new Options();
        Option optEbook = new Option("e", "ebook", true, "ebook name (must be in ALEXANDRIABOOKSENG-METADATA");
        optEbook.setRequired(false);
        options.addOption(optEbook);

        //This option needs to be PRE validated (missing right now)
        //1-check if metadata has audio-url with data
        //2-check if epub is in the temp folder to enable extraction
        Option optExtract = new Option("x", "extract", true, "Extract audio and toc metadata");
        optExtract.setRequired(false);
        options.addOption(optExtract);

        //Same with the combiner. Combiner depends directly from extract and files need to exist in some places (mp3.json, toc.json) before proceed.
        Option optCombiner = new Option("c", "combiner", false, "Combines audio and text making a new enhanced epub");
        optCombiner.setRequired(false);
        options.addOption(optCombiner);

        //New option to check metadata and files properly without processing anything.
        Option optMetadata = new Option("m", "metadata", true, "Checks metadata for a specific ebook, nothing else.");
        optMetadata.setRequired(false);
        options.addOption(optMetadata);

        Option optRepackage = new Option("r", "repackage", true, "Repackages the files combined and creates an epub with those files");
        optRepackage.setRequired(false);
        options.addOption(optRepackage);

        Option optIMDB = new Option("i", "imdb", true, "Generate the Internet Movie Database page from other/imdb/links-imdb.txt");
        optIMDB.setRequired(false);
        options.addOption(optIMDB);

        Option optASO = new Option("a", "aso", true, "Recover ASO descriptions from link in other/imdb/links-aso.txt");
        optASO.setRequired(false);
        options.addOption(optASO);


        Option optDefault = new Option("d", "default", true, "default an option to avoid answering y or not to everything");
        optDefault.setRequired(false);
        options.addOption(optDefault);

        Option optLevel= new Option("l", "level", true, "level checking (urgca).Goes with metadata options");
        optLevel.setRequired(false);
        options.addOption(optLevel);

        Option optClean = new Option("clean","clean", true, "clean specific text from pages");
        optClean.setRequired(false);
        options.addOption(optClean);

        Option opLines = new Option("lines","lines", true, "how many lines to clean");
        opLines.setRequired(false);
        options.addOption(opLines);

        Option opcfStructure = new Option ("cfs", "createfolderstructure", true,"Create folder structure for a new ebook");
        opcfStructure.setRequired(false);
        options.addOption(opcfStructure);

        Option opcTitle = new Option ("title", "title", true,"title for the ebook");
        opcTitle.setRequired(false);
        options.addOption(opcTitle);

        Option opcAuthor = new Option ("author", "tiautauthorhor", true,"author for the ebook");
        opcAuthor.setRequired(false);
        options.addOption(opcAuthor);

        Option opcAuthorMeta = new Option ("authormeta", "authormeta", true,"link for the author biography");
        opcAuthorMeta.setRequired(false);
        options.addOption(opcAuthorMeta);

        Option opcBookUrl = new Option ("bookurl", "bookurl", true,"likk for the ebook, usually gutenberg link");
        opcBookUrl.setRequired(false);
        options.addOption(opcBookUrl);

        Option opGetEpub = new Option ("ge", "getepub", true,"Get the epub from the link provided in metadata.txt");
        opGetEpub.setRequired(false);
        options.addOption(opGetEpub);

        Option opBiography = new Option ("b", "biography", true,"Generates biography from an ID author of  GoodReads site");
        opBiography.setRequired(false);
        options.addOption(opBiography);

        Option opBioLink = new Option ("biolink", "biolink", true,"link for generating the proper biography folder in authors directory");
        opBioLink.setRequired(false);
        options.addOption(opBioLink);

        //addmeta

        Option opAddmeta = new Option ("addmeta", "addmeta", true,"adds or replace a parameter in the metadata from ebook.");
        opAddmeta.setRequired(false);
        options.addOption(opAddmeta);

        Option opAddmetaParameter = new Option ("p", "p", true,"Parameter in the format name=value");
        opAddmetaParameter.setRequired(false);
        options.addOption(opAddmetaParameter);

        Option opEverything = new Option ("everything", "everything", true,"Makes the $everything folder for an ebook");
        opEverything.setRequired(false);
        options.addOption(opEverything);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd =null;

        try {
            cmd = parser.parse(options, args);
            if (cmd.getOptions().length==0) {
                System.out.println("Available parameters:");
                System.out.println("-e / -ebook [ebook]     : Process ebook and generate compiled.apk");
                System.out.println("-x / -extract [ebook]   : Extract metadata and generates mp3.json/toc.json, leaving extraction on temp folder");
                System.out.println("-c / -combiner          : Merge mp3.json and toc.json on temp folder creating an AudioEbook epub");
                System.out.println("Combiner command ONLY WORKS FOR EXTRACTED BOOKS (you did -x previously AND have audio ready to combine)");
                System.out.println("-r / -repackage [ebook] : Repackages epub from the epub folder data. Backups mp3.json / toc.json when possible");
                System.out.println("-m / -metadata [ebook] -l/level (urgca) : Check metadata for a specific ebook, does nothing else");
                System.out.println("        u - check what we need to unpackage");
                System.out.println("        r - check what we need to package");
                System.out.println("        g - check what we need to upload the app to Google Play");
                System.out.println("        c - check what we need to compile the epub");
                System.out.println("        a - (all). Check everything");
                System.out.println("-i / -imdb [ebook]      : Creates the imdb page based on links provided on !links-imdb...!end section on metadata.txt");
                System.out.println("                          Important: -extract ALREADY CALLS -imdb command so this command could be useless now");
                System.out.println("-a / -aso [ebook]       : Creates an ASO.txt on links provided on !links-aso...!end section on metadata.txt");
                System.out.println("                          Important: -extract ALREADY CALLS -aso command so this command could be useless now");
                System.out.println("-d / -default y/n       : Default answer to yes or no. Used to avoid answering repetitive questions... ");
                System.out.println("-clean [string-to-find] -lines [how-many-lines]                  : Clean a file from one match to n lines");
                System.out.println("Clean command ONLY WORKS FOR EXTRACTED BOOKS (you did -x previously)");
                System.out.println("Clean example for adelaide books: -clean docfoot -lines 8");
                System.out.println("-b / -biography [id] : Creates a biography based on a id from GoodReads");
                System.out.println("Additional parameter (mandatory):");
                System.out.println("-biolink : String used to generate a a folder in authors directory. For example for Victor Hugo it could be victor-hugo");
                System.out.println("*****************************************************************************************");
                System.out.println("-cfs / -createfolderstructure [ebook] : Create folder structure for a new ebook");
                System.out.println("Optional parameters:");
                System.out.println("-title [title] = the ebook's title (will be put in metadata)");
                System.out.println("-author [author] = the ebook's author (will be put in metadata)");
                System.out.println("-authormeta [meta] = the ebook's author link (will be put in metadata)");
                System.out.println("-bookurl [bookurl] = the ebook's url, usually a ebook-gutenberg-link (will be put in metadata)");
                System.out.println("*****************************************************************************************");
                System.out.println("-ge / -getepub [ebook] : tries to get the epub from the link provided in metadata.txt, ebook-gutenberg-link");
                System.out.println("-addmeta [ebook] -p variable=value : adds or replaces a variable in the metadata with the corresponding data");
                System.out.println("Note 1: New variables are written to the end of the file. If variable exists, the data is updated.");
                System.out.println("Note 2: parameter name CAN'T have spaces in it");
                System.out.println("Note 3: if value has spaces, delimit value with double comillas. Example -p =\"parName=my value with spaces\"");
                System.out.println("Note 4: Simple comilla is not valid as delimiter, but you can use in the parameter value: -p \"parName=House's Name\"");
                System.out.println("*****************************************************************************************");
                System.out.println("-everything [ebook]: makes the folder $everything with all necessary to upload an app to Google Play");
                System.out.println("This command does the following:");
                System.out.println ("1-creates folder $everything from scratch (or empties it)");
                System.out.println ("2-Copy the latest apk to $everything");
                System.out.println ("3-copies cover.jpg,icon-512x512.png/jpg and icon-1024x500.png/jpg");
                System.out.println ("4-copies all files from ebook000/common-screenshots EXCEPT .psd files");
                System.out.println ("(please use this folder for putting any file needed to be copied)");
                System.out.println ("5-copies author screenshot from biography");

            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        }


        if (cmd.hasOption("default")) {
            defaultAnswer =cmd.getOptionValue("default").toLowerCase();
            if (!defaultAnswer.equals("y") && !defaultAnswer.equals("n")) {
                System.out.println("Unknown default option: should be (Yy) or (Nn) only");
                defaultAnswer ="";
            }
            else
            {
                System.out.println("Defaults all answers to " + defaultAnswer);
            }
        }

        //Parameters with no data, needs the hasOption to check if they are there.
        if (cmd.hasOption("ebook")) {
            String ebook = cmd.getOptionValue("ebook");
            packageEbook(ebook);
            return; //This is the only task, don't try doing anything else
        }

        if (cmd.hasOption("extract")) {
            String ebook = cmd.getOptionValue("extract");
            unpackageEpub(ebook);
            return;
        }

        if (cmd.hasOption("combiner")) {
            //removeAudioEpub - a simple way of cleaning up before start.
            // Recall removeAudioEpub can't currently solve glitches as putting several times audio tags
            /**Recombine and create audio tags inside epub from mp3 and toc metadata created*/
            removeAudioEpub(TOC_JSON_PATH, REGEXP_BODY_TAG, BODY_TAG_RESET);
            addAudioEpub(TOC_JSON_PATH, MP3_JSON_PATH, REGEXP_INSERT_AUDIO,REGEXP_BODY_TAG);
            System.out.print("DONE!");
        }

        if (cmd.hasOption("metadata")) {
            String ebook = cmd.getOptionValue("metadata");
            String vL="";
            if (cmd.hasOption("level")){
                vL = cmd.getOptionValue("level").toLowerCase();
            }
            else
            {
                System.out.println("level option not found. Assuming ALL");
                vL="a";
            }
            boolean flag=true;
            if (!"urgca".contains(vL)){
                 System.out.println("\ninvalid option: level parameter invalid or empty : " + vL);
             } else
             {
                 Config c = new Config(ebook);

                 switch (vL)
                 {
                     case "u":
                         flag=c.isConfigurationValid(ebook,validationLevel.UNPACKAGE);
                         break;
                     case "r":
                         flag=c.isConfigurationValid(ebook,validationLevel.REPACKAGE);
                         break;
                     case "g":
                         flag=c.isConfigurationValid(ebook,validationLevel.GOOGLE_PLAY);
                         break;
                     case "c":
                         flag=c.isConfigurationValid(ebook,validationLevel.COMPILE);
                         break;
                     case "a":
                         flag=c.isConfigurationValid(ebook,validationLevel.ALL);
                         break;
                 }
             }
            //boolean flag= Config.ebookMetadataValidation(ebook);
            //boolean flag= Config.isConfigurationValid(ebook,);
            if (flag) {
                System.out.println("\nMETADATA CHECK: -->PASS<--\n");
                return;
            }
            System.out.println("\nMETADATA CHECK: -->FAIL<--\n");
            return;
        }

        if (cmd.hasOption("repackage")) {
            String ebook = cmd.getOptionValue("repackage");
            repackageEbook(ebook);
            return;
        }

        if (cmd.hasOption("imdb")) {
            String ebook = cmd.getOptionValue("imdb");
            generateIMDB(ebook);
        }

        if (cmd.hasOption("aso")) {
            String ebook = cmd.getOptionValue("aso");
            generateASO(ebook);
        }

        if (cmd.hasOption("clean")) {
            String match =cmd.getOptionValue("clean");
            if (cmd.hasOption("lines")) {
                String lines = cmd.getOptionValue("lines");
                int l = Integer.parseInt(lines);
                cleanPages (match,l);
            } else
            {
                System.out.println(("ERROR: command clean needs parameter lines to operate"));
            }
        }

        if (cmd.hasOption("createfolderstructure")) {
            String title,author,authormeta,bookurl;
            String ebook = cmd.getOptionValue("createfolderstructure");
            EbookData eb = new EbookData(ebook);

            if (cmd.hasOption("title")) {
                title=cmd.getOptionValue("title");
                eb.setTitle(title);
            }
            if (cmd.hasOption("author")) {
                author=cmd.getOptionValue("author");
                eb.setAuthor(author);
            }

            if (cmd.hasOption("authormeta")) {
                authormeta=cmd.getOptionValue("authormeta");
                eb.setAuthormeta(authormeta);
            }

            if (cmd.hasOption("bookurl")) {
                bookurl=cmd.getOptionValue("bookurl");
                eb.setBookurl(bookurl);
            }

            createEbookFolder(eb);

        }

        if (cmd.hasOption("getepub")) {
            String ebook = cmd.getOptionValue("getepub");
            getGutenbergEpub(ebook);
        }

        if(cmd.hasOption("biography")) {
            String biography =cmd.getOptionValue("biography");
            String biolink;

            if (cmd.hasOption("biolink")) {
                biolink =cmd.getOptionValue("biolink");
                generateBio(biography,biolink);
            } else
            {
                System.out.println("ERROR: missing biolink, unable of making folder structure for author id = " + biography + " ,aborting...");
                return;
            }

        }

        if(cmd.hasOption("addmeta")) {
            String ebook = cmd.getOptionValue("addmeta");
            String parameter;
            if (cmd.hasOption("p")) {
                parameter = cmd.getOptionValue("p");
                Config.writeMetadataParameter(ebook,parameter);
            } else {
                System.out.println("ERROR: missing parameter (-p name=value) , aborting...");
                return;
            }

        }

        if (cmd.hasOption("everything")) {
            String ebook= cmd.getOptionValue("everything");
            makeEverythingFolder(ebook);
        }

    }

    /*
    Make it's best of getting all data needed to simplify the process of uploading to Google Play
     */
    private static void makeEverythingFolder(String ebook) throws IOException, InterruptedException {
        //Paso 1: Verificar si existe la carpeta.
        String Destination = String.format(EBOOK_EVERYTHING,ebook);
        File dstDir = new File(Destination);
        if (dstDir.exists()) {
            //Clear the folder before begin
            boolean flag = YesNo("There is already a folder $everything for " + ebook +", delete(Y/N)?",
                    "You decided not to recreate, aborted",
                    "Deleting $everything folder for " + ebook);
            if (!flag) return;
            dstDir.delete();
            System.out.println("Creating dir $everything for" + ebook);
            dstDir.mkdir();
        } else {
            dstDir.mkdir();
        }
        //Paso 2: Copiar apk a este directorio.
        File apk = getLatestApk(ebook);
        if (apk ==null) {
            System.out.println("Error: there is no apk available, try compiling one on:"  + ebook);
        } else {
            FileUtils.copyFileToDirectory (apk,dstDir);
            System.out.println("Apk " + apk.toString() + " copied");
        }
        //Paso 3: Copiar imagenes necesarias para publicacion
        //Estas son especificamente: cover.jpg , icon 512x512 , icon 1024x512
        File coverFile = new File(String.format(EBOOK_COVER,ebook) + "cover.jpg");
        if (!coverFile.exists()) {
            System.out.println("Warning: cover.jpg does not exist");
        } else {
            FileUtils.copyFileToDirectory(coverFile,dstDir);
            System.out.println("cover.jpg copied");
        }

        File icon512 = new File(String.format(EBOOK_ICONS,ebook) + "icon-512x512.png");
        File icon512jpg = new File(String.format(EBOOK_ICONS,ebook) + "icon-512x512.jpg");
        if(icon512.exists()) {
            FileUtils.copyFileToDirectory(icon512,dstDir);
            System.out.println("icon512x512.png copied");
        } else {
            if(icon512jpg.exists()) {
                FileUtils.copyFileToDirectory(icon512jpg,dstDir);
                System.out.println("icon-512x512.jpg copied");
            } else {
                System.out.println("Warning: icon-512x512 (png/jpg) not found");
            }
        }

        File icon1024 = new File(String.format(EBOOK_ICONS,ebook) + "icon-1024x500.png");
        File icon1024jpg = new File(String.format(EBOOK_ICONS,ebook) + "icon-1024x500.jpg");
        if(icon1024.exists()) {
            FileUtils.copyFileToDirectory(icon1024,dstDir);
            System.out.println("icon1024x500.png copied");
        } else {
            if(icon1024jpg.exists()) {
                FileUtils.copyFileToDirectory(icon1024jpg,dstDir);
                System.out.println("icon-1024x500.jpg copied");
            } else {
                System.out.println("Warning: icon-1024x512 (png/jpg) not found");
            }
        }
        //Paso 4: copiar imagenes de base siempre estan en todos los ebooks
        //ebook000\common-screenshoots\ se asume que existe sino, habra error
        //Ojo que copia archivo psd innecesario.
        System.out.println("copying additional screeshots,wait...");
        File screenShots = new File(EBOOK_COMMON_SCREENSHOTS);
        File[] screenshotImages = screenShots.listFiles();
        String fDest ="";
        if (screenshotImages == null) {
            System.out.println("ERROR: there is not common screenshots to copy");
            System.out.println("Copy from common screenshots aborted");
        }
        else
        {
           for (File f: screenshotImages) {
                //Ignore everything but png files
                if (f.getName().endsWith("psd")) continue;;
                fDest = dstDir.toString() +"\\" + f.getName();
                FileUtils.copyFile(f,new File(fDest));
                Thread.sleep(1000);
            }
        }
        System.out.println("copying biography screenshot...");
        //Paso 5 y ultimo: copiar screenshot(s) de biografia al folder.
        Config c = new Config(ebook);
        String biographyPath = String.format(EBOOK_AUTHORS , c.biography );
        File bioFileImages = new File(biographyPath + "screenshots");
        File[] bioImages = bioFileImages.listFiles();
        fDest ="";
        if (bioImages == null) {
            System.out.println("ERROR: this ebook has an invalid biography:" + c.biography);
            System.out.println("Image from biography aborted");
        } else
        {
            for (File f: bioImages) {
                fDest = dstDir.toString() +"\\" + f.getName();
                FileUtils.copyFile(f,new File(fDest));
                Thread.sleep(1000);
            }
            System.out.println("biography screenshot copied");
        }
        System.out.println("Make $everything folder finished!");

    }

    private static void copyToEverything(String ebook, String filePath) throws IOException {
        String dstDir = String.format(EBOOK_EVERYTHING,ebook);
        //Note: this will overwrite any existing file.
        FileUtils.copyFileToDirectory(new File(filePath), new File(dstDir));
    }
    /*
    Creates a biography accessing GoodReads site and retrieving author information.
    The process needs many steps:
    1-Create author folder. If folder exists???
    2-Retrieve data. This step is made FIRST to avoid making folders which could be empty if author is missing
    3-Save author image to image folder.
    4-Resize image to standard.
    5-Run template for biography
    6-Save biography data
    7-End process.
     */
    private static void generateBio (String biography,String bioLink)  {
        //Step 1: First retrieve data if that is possible
        AuthorData ad = GoodReadsWrapper.getAuthorData(Integer.parseInt(biography));
        //2-Try to make the folder if possible
        if (!createAuthorFolder(ad,bioLink)) return;
        //3-try getting images and downloading properly , but download to a temporal folder
        String imageName= FilenameUtils.getBaseName(ad.urlImage);
        //String imageName= FilenameUtils.getName(ad.urlImage);
        ImageTools.downloadUrlImage(ad.urlImage, TMP_FOLDER + imageName);
        //4-Try resizing image and put in the destination
        String imgDestination =  imageName + "-400x565.jpg";
        try {
            ImageTools.resize(TMP_FOLDER + imageName,400,565,
                    EBOOK_AUTHORS_ROOT + bioLink + "/Images/" + imgDestination);
            //5-Finally run the template and save biography data
            ad.finalImage = imgDestination;
            executeTemplate(ad,"biography.xhtml.mustache",EBOOK_AUTHORS_ROOT + bioLink + "/Text/biography.xhtml");
            System.out.println("Biography for Id=" + biography + "(" + ad.name + ") created in authors/" + bioLink);
        }
        catch (IOException e)
        {
            System.out.println("ERROR IO Exception:" + e.getMessage());
        }
    }



    //Tries to automatically get the epub from Gutenberg throught the link provided.
    //The download link is made using the know how provided here lookign how rdf files are made
    // Se the index file on http://www.gutenberg.org/wiki/Gutenberg:Feeds

    private static void getGutenbergEpub(String ebook) throws IOException {
        Config c;
        try {
            c = new Config(ebook);
        } catch (Exception e)
        {
            System.out.println("ERROR: " + e.getMessage());
            System.out.println("SUGGESTION: Please check of folder for " + ebook + " does exists!!!");
            return;
        }



        String  cl =c.gutenberglink.trim();
        if (cl == EpubTools.METADATA_NOT_FOUND) {
            System.out.println("The link was not found inside metadata.txt on ebook " + ebook);
            System.out.println("Please add ebook-gutenberg-link variable inside " + ebook);
            return;
        }

        if (cl == EpubTools.METADATA_EMPTY) {
            System.out.println("ebook-gutenberg-link is empty! ( in " + ebook +")");
            System.out.println("Please add ebook-gutenberg-link value...");
            return;
        }

        if (cl.length()==0) {
            System.out.println("ebook-gutenberg-link is undefined! ( in " + ebook +")");
            System.out.println("Please add ebook-gutenberg-link value...");
            return;

        }
        //Build download url
        String[] parts = cl.split("/");
        if (parts.length<1) {
            System.out.println("ERROR: url is not in the proper format to extract book number:" + cl+  " . Aborting...");
            return;
        }
        String bookNumber =parts[parts.length-1];//get the last part

        //Make download link
        String url1 = GUTENBERG_SITE_URL + bookNumber + ".epub.images";
        String url2 = GUTENBERG_SITE_URL + bookNumber + ".epub.noimages";
        // File ebookFolder = new File(String.format(EpubTools.EBOOK_EPUB,ebook));

        boolean downloaded =  downloadFile(url1,ebook, ebook+ ".images.epub");
        if (!downloaded) {
            System.out.println("Unable to download epub file using " + url1 + " . Trying with no images..." );
            downloaded =  downloadFile(url2,ebook, ebook + ".noimages.epub");
            if (downloaded) {
                System.out.println("Downloaded epub file using " + url2 + " (no images)");
            } else {
                System.out.println("Unable to download epub file using " + url2 );
            }
        } else {
            System.out.println("Downloaded epub file using " + url1 + "(with images)");
        }

        if (!downloaded) {
            System.out.println("ERROR: unable to download Ebook in epub format...SORRY!");
        }
    }

    private static boolean downloadFile(String urlString, String ebook, String filename) throws MalformedURLException {
        Boolean downloaded =false;
        URL url = new URL(urlString);
        try
        {
            FileUtils.copyURLToFile(url,new File(String.format(EpubTools.EBOOK_EPUB,ebook) + "/" + filename));
           return true;
        }
        catch (IOException e) {
            System.out.println("ERROR when downloading epub:" + e.getMessage());
            return false;
        }
    }
    /**
     * Creates a folder structure.
     * The basic idea is copying the structure from  ebook000/ebook-structure
     * After that, it should be a good idea to add some script to automatize metadata.txt

     */

    public static void createEbookFolder(EbookData ed) throws IOException {
        //if folder already exists, return an error
        File srcDir = new File(EBOOK_FOLDER_STRUCTURE);
        File dstDir = new File(ALEXANDRIA_ROOT + ed.ebook);
        if (dstDir.exists()) {
            System.out.println(("ERROR: folder " + ed.ebook + " already present, aborting..."));
            return;
        }
        try {

            System.out.println("Creating dir " + ed.ebook);
            FileUtils.copyDirectory(srcDir, dstDir);
            System.out.println("DONE copy, updating metadata.txt...");
            String metadataFilePath = ALEXANDRIA_ROOT + ed.ebook + "/metadata.txt";
            executeTemplate(ed,"metadata.txt.mustache",metadataFilePath );
            System.out.println("Metadata updated, process ended!");

        } catch (IOException e) {
            System.out.println(("ERROR when creating directory " + ed.ebook + ": " + e.getMessage()));
        }
    }

    /*
    This current version aborts if  author folder already exists.
     */
    public static boolean createAuthorFolder(AuthorData ad, String biolink) {
        //if folder already exists, return an error
        File srcDir = new File(AUTHOR_FOLDER_STRUCTURE);
        File dstDir = new File(EBOOK_AUTHORS_ROOT + biolink +"/");
        if (dstDir.exists()) {
            boolean flag = YesNo("There is already a folder with " + biolink +" name, delete(Y/N)?","You decided not to recreate, aborted","Deleting current folder and recreating again");
            if (!flag) return false;
            dstDir.delete();
            System.out.println("Directory deleted");
        }
        try {

            System.out.println("Creating dir " + biolink);
            FileUtils.copyDirectory(srcDir, dstDir);
            System.out.println("DONE copy, now creating biography...");
            return true;

        } catch (IOException e) {
            System.out.println(("ERROR when creating directory " + biolink + ": " + e.getMessage()));
        }
        return false;
    }


    /**
     * This tools cleans all pages from some text starting from one match.
     * This is used to clear( specially) annoying footers. It only works in the Temp/epub/OEBPS/Text/*.xml,*.xhtml or *.html files.
     * @param match
     * @param l
     */
    public static void cleanPages (String match, int l) throws IOException {
//Paso 1: obtener lista de archivos que vamos a procesar.
        String folder = TMP_FOLDER + EPUB_FOLDER + "Text";
        //Paso 2: obtener la lista de archivos en esta carpeta.
        File d = new File(folder);
        if (!d.isDirectory()) {
            System.out.println("ERROR: " + d.toPath() + " missing");
        }
        List<String> ext = new ArrayList<String>();
        ext.add("html");
        ext.add("xhtml");
        ext.add("htm");
        ext.add("xml");


        File[] files = d.listFiles();
        //Recorrer los archivos
        String extension = "";
        for (File f : files) {
            extension = FilenameUtils.getExtension(f.getName());
            if (ext.contains(extension)) {
                cleanPage(f, match, l);
            }
        }
    }

    public static void cleanPage (File f, String match, int l ) throws IOException {
        //Abrir el archivo
        System.out.println("Cleaning " + f.getName() + "...");
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f.toPath().toString()), "UTF-8"));
        StringBuffer sb = new StringBuffer();
        //I'm using found and finished to apply just only one time per file.
        Boolean found=false;
        Boolean finished = false;
        String line="";

        while ((line = br.readLine()) != null)
        {
            if (line.contains(match)) {
                found = true;
            }
            if (finished || !found ) {
                sb.append(line + "\n");
                continue;
            }
            l--;
            if (l==0) {
                finished = true;
            }
        }
        br.close();
        if (found) {
            System.out.println("Found match " + match  +". supressing lines...");
        } else
        {
            System.out.println("Nothing found...");
            return; //don't change files at all.
        }

        BufferedWriter bwr =getWriter(f.toPath().toString());
        bwr.write(sb.toString());
        bwr.flush();
        bwr.close();

    }

    /**
     * Generate ASO.txt file used for creating ASO descriptions .
     * It uses other/imdb/links-aso.txt to automatize it
     * #lines are ignored, also empty ones.
     * All the links provided needs to point to an app on Google Play Store
     * @param ebook
     * @throws IOException
     */

    public static void generateASO(String ebook) throws IOException {
        String basePath =String.format(EBOOK_OTHER,ebook) + "imdb/";
        String asoPage = basePath +"page/ASO.txt";
        File page = new File(basePath +"page/");
        AsoList asoList = new AsoList();
        if (!page.isDirectory()) {
            System.out.println("Error: Directory missing:" + basePath +"page/.\nProcess aborted.");
            return;
        }

        String descriptionASO;
        //regular expresion for finding ASO description
        Scraper s = new Scraper(); //imdb scrapper
        String appId="";

        //Also delete the remaining images...
        int asoCounter=1;

        String lines[] = Config.getMetadataParameterArray(ebook,"links-aso");

        for (String line : lines ){
            try {
                appId = line.split("=")[1];
            } catch (Exception e) {
                System.out.println("ERROR: Unable of getting appId from url " + line);
            }
            System.out.println("Extracting data from Google Play for id=" + appId + " please wait...");
            descriptionASO =s.findDescription(line.trim());
            //Add carriage return since we're going to use text files not html
            descriptionASO = descriptionASO.replaceAll("<br>", "<br>\r\n");
            if (!descriptionASO.equals("")) {
                //Adds description to object
                asoList.addASO( String.valueOf(asoCounter), descriptionASO );
                asoCounter++;
            } else
            {
                System.out.println("ERROR: unable of getting ASO from url:" + line );
            }
        }


        //Process asoItem list against the template
        executeTemplate(asoList, "asolist.mustache", asoPage);
        System.out.println("Generating ASO.txt finished");
        System.out.println("Check " + asoPage);
        boolean flag = YesNo("Open the page in browser?(Y/N)","You decided not to open the page, finished!","");
        if (!flag) return;
        openInBrowser(asoPage); //In browser? Better in notepad
        copyToEverything(ebook, asoPage); //copy the ASO generated to $everything folder.
    }

    /**
     * Generates the imdb.xhtml page from a list of links provided from other/imdb/links-imdb.txt for a particular ebook
     * Improved: imdb links embedded on metadata.txt , unifying dispersed data from once for all.
     * This is the default switch used . However for generating the page inside the ebook, another method
     * is provided, for specifying folders.
     * 20180924: If there are few links or none, warn them and optionally check user what to do.
     * @param ebook
     */

    public static void generateIMDB (String ebook) throws Exception {
        String basePath =String.format(EBOOK_OTHER,ebook) + "imdb/";
        String folderImg = basePath + "Images/"; //Location where the images will be saved
        String folderHtml = basePath + "page/";
        generateIMDB(ebook,folderHtml,folderImg);
    }
    //        executeTemplate(c, "title.xhtml.mustache", TMP_FOLDER + EPUB_FOLDER + "Text/title.xhtml");
    // folderHtml = TMP_FOLDER+EPUB_FOLDER + "Text";
    //folderImg  =TMP_FOLDER+EPUB_FOLDER + "Images";

    /**
     * given an ebook, and a html folder and image folder, it will generate a proper imdb.xhtml page
     * saving the images on the corrsponding folder.
     * 20180924: Improved: abort if there are no links (many ebooks doesn't have imdb pages)
     * 20180924: Improved: check to continue if there are few links (probably an error).
        20180926: No matter what I do, the system REFUSES completely to delete the unnneeded images.
     So I change the approach, leaving the images in a temporal folder and the copying only the needed files.
     20180927: If ound that IMDB cut improperly the images son I decide select the image using aspect ratio.
     * @param ebook
     * @param folderHtml
     * @param folderImg
     * @throws IOException
     */
    public static void generateIMDB(String ebook, String folderHtml, String folderImg) throws Exception {

        String imdbPage = folderHtml + "imdb.xhtml";
        File images=new File(folderImg);
        File page = new File(folderHtml);

        if (!page.isDirectory()) {
            System.out.println("Error: Directory missing:" +folderHtml + ".\nProcess aborted.");
            return;
        }

        String regExpId ="/(tt.*?)/"; //regular expresion to find imdb id.
        Pattern p = Pattern.compile(regExpId);
        Matcher m;
        Scraper s = new Scraper(); //imdb scrapper
        String id="";
        imdbList movieList = new imdbList(); //list used against mustache template
        movieList.bookname = Config.getMetadataParameter(ebook,"ebook-name").trim();
        String title="";
        String ext ="";
        File img1;
        File img2;
        File img3;

        //Read imdb links from metadata
        String lines[] = Config.getMetadataParameterArray(ebook,"links-imdb");
        if (lines.length==0){
            System.out.println("WARNING: no links found for processing imdb file. Check if this is correct....!");
            return;
        }

        if (lines.length<3){
            if (YesNo("WARNING: There are too few links to generate the IMDB link. Abort IMDB page generation?(Y/N)",
                    "","Aborted IMDB generation per user request"));
            return;
        }

        for (String line : lines ){
            m =p.matcher(line.trim()); //Find id inside link   using /(tt.*?)/
            if (!m.find()) {
                System.out.println(("WARNING: no Id found on link:"  + line));
                continue;
            }
            id =m.group(1);
            System.out.println("Extracting data from IMDB for id=" + id + " please wait...");
            s.findById(id);
            if (!s.hasFound()) {
                System.out.println("ERROR:" + s.getId() + " doesn't have data on IMDB");
                continue;
            }
            //Normalize description for renaming image file

            title =s.getTitle().trim().replaceAll("[:()'\"$%&-\\.\\s]*","").toLowerCase();
            //title =title.replace(" ","-");

            ext=FilenameUtils.getExtension(s.getPoster()); //Get file extension to decide format
            img1=new File(EBOOK_TRASH_FOLDER + title + "." + ext);
            img2=new File(EBOOK_TRASH_FOLDER + title + "_big." + ext);
            img3=new File(folderImg + title + "-336x475." + ext);

            //Get two files and decide the biggest for  better resizing
            //Tactic change

            s.downloadPoster(img1);
            s.downloadPoster(img2,true);
            Dimension dimg1 = ImageTools.getImageDimension(img1);
            int aspectRatio = dimg1.height/ dimg1.width; //Normal Image ratio should be in the order of aspectRatio>1 or 1.5
            if (aspectRatio>1) {
                ImageTools.resize(img1, IMDB_IMG_WIDTH, IMDB_IMG_HEIGHT,img3);
            } else {
                ImageTools.resize(img2, IMDB_IMG_WIDTH, IMDB_IMG_HEIGHT,img3);
            }
            //Add asoItem to the list
            movieList.addMovie(s.getId(),s.getTitle(),s.getUrl(),"../Images/" + img3.getName());
            XmlTools.addFileToContentOPF("Images",img3.getName());
            //Remove auxiliary files they are not needed any longer. However, it does not work. Why?
            //executeCommandLine(folderImg,"del " + img2.getName() + " /Q","Deleting " + img2.getName() + "...");
            //executeCommandLine(folderImg,"del " + img1.getName() + " /Q","Deleting " + img1.getName() + "...");
           // toDelete.add(img1);
            //toDelete.add(img2);
        }

        //Process asoItem list against the template
        executeTemplate(movieList, "movies.xhtml.mustache", imdbPage);
        XmlTools.addFileToContentOPF("Text","imdb.xhtml");
        System.out.println("Generating imdb.xhtml finished");
        System.out.println("Check " + imdbPage);

        boolean flag = YesNo("Open the page in browser?(Y/N)","You decided not to open the page, finished!","");
        if (!flag) return;
        openInBrowser(imdbPage);
        //https://stackoverflow.com/questions/5226212/how-to-open-the-default-webbrowser-using-java

   }


    static void openInBrowser(String url) throws IOException {
        Runtime rt = Runtime.getRuntime();
        rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
    }

    /**
     * This method does the following non-trivial tasks:
     * 1-Get ebook name (from ebook metadata).
     * 2-Zips the current ebook with the proper ebook name
     * REJECTED 3-Checks epub folder and finds out the best current name
     * 4-Copies the epub to the correct folder
     * 5-Preserves mp3.json and specially toc.json to the other\json folder. Checks if it needs overwritting.
     * @param ebook
     * @return
     */
    public static File repackageEbook (String ebook) {
        //1-get metadata name for ebook
        String ebookName = null;
        try {
            ebookName = Config.getMetadataParameter(ebook,"ebook-name");
        } catch (IOException e) {
            System.out.println("repackageEbook error: can't get MetadataParameter for ebook:" + ebook + ",parameter: ebook-name");
            System.out.println("Additional info:" + e.getMessage());
            System.out.println("Process aborted");
            return null;
        }
        //2-Create a unique filename (includes a postfix with time creation
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
        //Create an unique name which also give when the file was created.
        //reduce book name, strip away some characters.
        String nameReduce = ebookName.toLowerCase()
                            .trim()
                            .replace(" ","-")
                            .replace(",","")
                            .replace(":","")
                            .replace(".", "") +
                            "-" +
                            timeStamp + ".epub";
        String filename = ebook + "-" + nameReduce;

        //3-Zip the folder  under the temp folder
        System.out.println("Compressing " + filename + "...");
        try {
            ZipTools.zipDir(TMP_FOLDER + "/epub",TMP_FOLDER+ "\\" + filename);
        } catch (Exception e) {
            System.out.println("zipDir error: " + e.getMessage());
            System.out.println("Process aborted");
            return null;
        }
        System.out.println("Done compresion." );
        //4-Copies the file to the proper epub folder. (Note: chances of finding the same filename are extremely low)
        String copyTo = String.format(EBOOK_EPUB,ebook);
        File destination = new File(copyTo +  filename);
        File newEpub = new File(TMP_FOLDER +  filename);
        try {
            Files.copy(newEpub.toPath(),destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Thread.sleep(2000);
            System.out.println("Epub copied to " + destination.toPath().toString() );
            Files.delete(newEpub.toPath()); //Delete the file fromo origin, otherwise it remains as garbage...
            Thread.sleep(1000);
            System.out.println("Epub source deleted (" + newEpub.toPath().toString()+")" );
        } catch (IOException e) {
            System.out.println("repackageEbook: error when copying to destination: " + e.getMessage());
            System.out.println("Copy aborted");
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //5-Preserves mp3.json and also toc.json to epub/other/json
        String destPath= String.format(EBOOK_OTHER,ebook + "/json");
        String mp3Dest = destPath + "mp3.json";
        String tocDest = destPath + "toc.json";

        //If files already exists, give to the user option to delete or not.
        pleaseOverride(MP3_JSON_PATH,mp3Dest,"mp3.json");
        pleaseOverride(TOC_JSON_PATH,tocDest,"toc.json");
        System.out.println("\nProcess repackageEbook ended successfully.");
        return destination;
    }

    /***
     * Ask user if override a file.
     * If user says Yes, the file is deleted, file is copied and true is returned
     * Otherwise, file is preserved, nothing is copied and false is returned.
     * @param sourcePathFile
     * @param destPathFile
     * @param msg
     * @return
     */
    public static boolean pleaseOverride(String sourcePathFile ,String destPathFile, String msg) {
        File destFile = new File(destPathFile);
        File srcFile = new File(sourcePathFile);
        boolean flag;
        if (destFile.isFile()){
            flag = YesNo("There is a "+ msg + " file already on destination!Overwrite(Y/N)",
                    "You decide NOT to overwrite, " + msg + " file process aborted","");
            if(!flag) return false; //If
            //Clear de file and continue
            try {
                FileUtils.forceDelete(destFile);
            } catch (IOException e) {
                System.out.println("pleaseOverride error: I couldn't delete file: " + destPathFile);
                System.out.println("Additional info:" + e.getMessage());
                return false;
            }
        }
        //Copy the file to destination
        try {
            FileUtils.copyFile(srcFile,destFile);
            Thread.sleep(2000);
            System.out.println(msg + " saved to " +  destPathFile);
            return true;
        } catch (IOException e) {
            System.out.println("pleaseOverride error: unable to copy from source to destination");
            System.out.println("Additional info: " + e.getMessage());
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace(); //I hope this error would never happen.... (thread.sleep)
        }
        return false;
    }

    /**
     * Simple Yes No mechanism. Return true if Yes otherwise false.
     * Improved: now it is possible pass a default answer to the script avoiding answering every time. See -d parameter.
     * @param msgYesNo
     * @param msgNo
     * @param msgYes
     * @return
     */
    public static boolean YesNo(String msgYesNo,String msgNo, String msgYes) {
        System.out.println(msgYesNo);
        String YN="";
        if (!defaultAnswer.equals("")) {
            YN = defaultAnswer;
        } else
        {
            Scanner scanner = new Scanner(System.in);
            YN = scanner.next(); // get their input as a String
        }

        if (!YN.toLowerCase().startsWith("y")) {
            if (!msgNo.equals("")) {
                System.out.println(msgNo);
            }
            return false; //Anything that is NOT Y would abort
        }
        if (!msgYes.equals("")) {
            System.out.println(msgYes);
        }
        return true;
    }
    /**
     * The following process are made:
     * 1-Copies the latest epub to temp file
     * 2-Unpackages to the epub folder
     * 3-generates mp3.json file. Checks if exists for overwriting
     * 4-generate toc.json . Checks if exist for overwriting
     * 20180918: extending unpackage, for including default pages that ALWAYS has to been there.
     * 20190925: changing audio-link tag for links-audio, following standard (links-imdb, links-aso, etc.)
     * @param ebook
     * @throws Exception
     */

    public static void unpackageEpub(String ebook) throws Exception {
        deleteFilesOnDestination(TMP_FOLDER); //Clear any epub file in the temp folder
        File destPath = copyEpubToDestination(ebook,TMP_FOLDER ); //Copy epub to tmp folder, return a reference to the copied file
                                                                  //to know what file to decompress
        FileUtils.deleteDirectory(new File(TMP_FOLDER + "epub")); //cleaning epub folder.
        //Notice I DON'T delete the mp3.json file nor toc.json. The second one can be customized and I don't want to accidentally destroy it.
        Thread.sleep(2000); //Wait to finish.
        //Unzip file to epub folder.

        ZipTools.unZipIt(destPath.toPath().toString(), TMP_FOLDER + "epub");

        String[] urlMp3 = Config.getMetadataParameterArray(ebook,"links-audio");
        if( urlMp3== null) {
            System.out.println("ERROR TAG links-audio MISSING: unable to retrieve audio link for build mp3.json");
        } else
        {
            if (urlMp3.length==0) {
                System.out.println("ERROR TAG links-audio has no info!: add links for building mp3.json");
            } else {
                extractMp3Url(ebook,urlMp3, MP3_JSON_PATH);
            }
        }



        extractTOC(TMP_FOLDER ,"toc.ncx", TMP_FOLDER + "toc.ncx"); //get toc to create toc.json
        tocJson(TMP_FOLDER + "toc.ncx", TMP_FOLDER + "toc.json");
        copyCustomPages(ebook);
        createDefaultCover(ebook);
        System.out.println(ebook + " processed. Next step: integrate audio.");
    }

    /**
     * Create a default cover and icons, which should be replaced later
     *
     * @param ebook
     */
    public static void createDefaultCover(String ebook) throws Exception {
        if (!YesNo("Should I create a default cover and icons?(Y/N)(It will existing ones)","Skipped cover & icons creation","")) return;
        //The bat should be executed on graphics path, since it create graphics on cover and icons folder.
        Config c = new Config(ebook);

        String execPath =ALEXANDRIA_ROOT + ebook + "/graphics/";
        executeCommandLine(execPath,"cover.bat " + "\"" + c.ebookname + "\" \"" + c.ebookauthor + "\""  ,"Creating cover and icons, please wait...");
        //After successful finish, it should copy cover to temp files in the proper place
        //FileUtils.copyDirectory(srcTitle,new File(TMP_FOLDER+EPUB_FOLDER + "Text/"));
        File dirTemp = new File(TMP_FOLDER + EPUB_FOLDER + "Text");
        if (!dirTemp.isDirectory()) {
            System.out.println("createDefaultcover ERROR: Destination dir missing:" + dirTemp.toPath().toString());
            return;
        }
        File ebookCover = new File(String.format(EBOOK_COVER,ebook) + "ebook-cover.jpg");
        if (!ebookCover.isFile())
        {
            System.out.println("createDefaultcover ERROR: cover not found:" + ebookCover.toPath().toString());
            return;
        }
        FileUtils.copyFile(ebookCover,new File(TMP_FOLDER + EPUB_FOLDER + "Images/ebook-cover.jpg"));
        XmlTools.addFileToContentOPF("Images","ebook-cover.jpg");
        Thread.sleep(2000);
    }

    /**
     * Copies custom pages always added to the all ebooks. This is optional and can be overrided with the -d (default) parameter
     * to Y or N.
     * Updated: EVERY item copied inside the ebook need to be added also to content.opf (normally in the epub root).
     * If we don't do this, the file stay 'invisible' because is not indexed.
     * Updated : fixed bug when generating imdb file.
     * 20181223: A small bug with unwanted consequences: added Styles and at least two files needed: title.css an page.css
     * (there is a dependency with the first to the second).
     * @param ebook
     */
    public static void copyCustomPages(String ebook) throws Exception {
        Boolean flag;
        //Directly abort if answer is no
        if (!YesNo("Copy extra custom pages inside the ebook?(Y/N)","Aborted. Custom pages ignored...","")) return;
        //Copy all fixed pages and images under ebook000 folder
        File srcTitle = new File(EBOOK_CUSTOM_PAGE + "Text/");
        if (!srcTitle.isDirectory()){
            System.out.println("ERROR: Src Dir missing: " + srcTitle.toPath().toString());
            return;
        }
        File srcImage = new File (EBOOK_CUSTOM_PAGE + "Images/");
        if (!srcImage.isDirectory()){
            System.out.println("ERROR: Src Dir missing: " + srcImage.toPath().toString());
            return;
        }

        File srcStyle = new File (EBOOK_CUSTOM_PAGE + "Styles/");
        if (!srcStyle.isDirectory()){
            System.out.println("ERROR: Src Dir missing: " + srcStyle.toPath().toString());
            return;
        }

        System.out.println("Copying html pages...");
        File[] sourceFiles =srcTitle.listFiles();
        String newDest="";
        for (File f: sourceFiles){
            newDest = TMP_FOLDER + EPUB_FOLDER +  "Text/" + f.getName();
            FileUtils.copyFile(f,new File(newDest));
            Thread.sleep(1000);
            XmlTools.addFileToContentOPF("Text",f.getName());
        }

        System.out.println("Copying images...");
        sourceFiles =srcImage.listFiles();

        for (File f: sourceFiles) {
            newDest = TMP_FOLDER + EPUB_FOLDER +  "Images/" + f.getName();
            FileUtils.copyFile(f,new File(newDest));
            Thread.sleep(1000);
            XmlTools.addFileToContentOPF("Images",f.getName());
        }

        System.out.println("Copying styles...");
        sourceFiles =srcStyle.listFiles();

        for (File f: sourceFiles) {
            newDest = TMP_FOLDER + EPUB_FOLDER +  "Styles/" + f.getName();
            FileUtils.copyFile(f,new File(newDest));
            Thread.sleep(1000);
            XmlTools.addFileToContentOPF("Styles",f.getName());
        }

        //Reconfigure title.xhtml according metadata and then copy to Title folder.
        generateTitlePage(ebook);

        generateIMDB(ebook,TMP_FOLDER + EPUB_FOLDER + "Text/",TMP_FOLDER + EPUB_FOLDER + "Images/");
        //To copy the biography, we need variables defined in the metadata.

        Config c = new Config(ebook);

        switch (c.biography){
            case METADATA_NOT_FOUND:
                System.out.println("ebook-biography not found in the metadata, can't copy or create biography file.");
                break;
            case METADATA_EMPTY:
                System.out.println("No metadata found, check ebook-biography and put an author from authors folder...");
                return;
            default:
                if (copyBiography(c)) {
                    System.out.println("ERROR!!!!: Biography data missing. Check correctness of ebook-biography tag!!");
                } else
                {
                    System.out.println("Biography data copied successfully");
                }
        }
        System.out.println("Copy custom pages, process finished!");
    }

    /**
     * Copies the biography for an specific author
     * 20180924: Improved: every file copied is updated on content.opf
     * @param c
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public static boolean copyBiography(Config c) throws Exception {
        String biographyPath = String.format(EBOOK_AUTHORS , c.biography );
        File bioFile = new File(biographyPath);
        if (!bioFile.isDirectory()) {
            System.out.println("biography not found for " + c.biography);
            return true;
        }
        //Copy files from biography to ebook.
        File bioFileText = new File(biographyPath + "Text");
        File bioFileImages = new File(biographyPath + "Images");
        if (!bioFileText.isDirectory()){
            System.out.println("Text directory not found in biography " + c.biography);
            return true;
        }

        if (!bioFileImages.isDirectory()){
            System.out.println("Images directory not found in biography " + c.biography);
            return true;
        }
        //Copy biography and images related.
        System.out.println("Copying biography data, please wait...");

        File[] bioTextes = bioFileText.listFiles();
        File[] bioImages = bioFileImages.listFiles();
        String fDest="";
        for (File f: bioTextes) {
            fDest = TMP_FOLDER+ EPUB_FOLDER + "Text/" + f.getName();
            FileUtils.copyFile(f,new File(fDest));
            Thread.sleep(1000);
            XmlTools.addFileToContentOPF("Text",f.getName());
        }

        for (File f: bioImages) {
            fDest = TMP_FOLDER+ EPUB_FOLDER + "Images/" + f.getName();
            FileUtils.copyFile(f,new File(fDest));
            Thread.sleep(1000);
            XmlTools.addFileToContentOPF("Images",f.getName());
        }

        /*
        FileUtils.copyDirectory(bioFileText,new File(TMP_FOLDER+EPUB_FOLDER + "Text/"));
        Thread.sleep(2000);
        System.out.println("Copying images...");
        FileUtils.copyDirectory(bioFileImages,new File(TMP_FOLDER+EPUB_FOLDER + "Images/"));
        Thread.sleep(2000);
        */
        return false;
    }

    /**
     * Everything included: going from epub to full apk compilation
     * @param ebook
     */
    public static void packageEbook(String ebook) throws Exception {
        boolean valid = Config.isConfigurationValid(ebook,validationLevel.COMPILE);
        if (valid){
            copyEpubToDestination(ebook, FR_EPUB_SOURCE);
            System.out.println("Creating icons...");
            createIcons(ebook);
            Thread.sleep(2000);
            System.out.println("Copying icons...");
            copyIcons(ebook);
            Thread.sleep(1000);
            reconfigureProject(ebook);
            clearapkbeforecompile();

            executeCommandLine(FOLIO_READER_ROOT, "gradlew assembleRelease", "\nSTARTING COMPILATION PROCESS!\n PLEASE WAIT FOR MESSAGES....");
            File apkDest=copyApkToEpubFolder(ebook);

            System.out.println("Packaging Ebook " + ebook + " finshed!!!!");
            sendApkToEmulator(apkDest);
        }
    }

    /**
     * installs apk to emulator (if possible). Include option
     * @param apk
     */
    private static void sendApkToEmulator(File apk) throws IOException, InterruptedException {
        // https://translate.google.es/#es/it/arroba
        if (!YesNo("Send apk to emulator?(Y/N)",
                     "You decided NOT to send it to the emulator",""))
        {
            return;
        }
        executeCommandLine(ALEXANDRIA_ROOT,"adb install -r " + apk.getPath().toString(),"Installing apk in emulator...");
    }
    /**
     * Currently release is always made here . We make sure of deleting
     * the apk before start, since it is possible compiler can check and do nothing
     * if apk is already there.
     */
    private static void clearapkbeforecompile() {
        File apkRelease = new File(FR_APK_RELEASE);
        apkRelease.delete();
        System.out.println("Clearing old apk (if there is one)...");
        }

    /**
     * Copies the file to destination, renaming the apk properly depending the ebook number & ebook name.
     * To improve: look for a way of increasing versions.
     * @param ebook
     * @throws IOException
     */
    private static File copyApkToEpubFolder(String ebook) throws IOException {
            System.out.println("Copying apk...");
            String ebookName = Config.getMetadataParameter(ebook,"ebook-name");
            String posfix = ebookName.trim().toLowerCase()
                    .replace(" ", "-")
                    .replace(":","")
                    .replace(",","");
            String destFilename = ebook + "-" + posfix + ".apk";
            File apkFrom = new File(FR_APK_RELEASE);
            //First check if directory exists
            File apkDirDest  = new File ( String.format(EBOOK_APK,ebook) );
            if (!apkDirDest.exists()) {
                apkDirDest.mkdir();
            }
            File apkDest  = new File ( String.format(EBOOK_APK,ebook) + destFilename);
            System.out.println("From:"  + apkFrom.getPath().toString());
            System.out.println(("To:" + apkDest.toPath().toString()));
            Files.copy(apkFrom.toPath(),apkDest.toPath(),StandardCopyOption.REPLACE_EXISTING);
           // copyToEverything(ebook,apkFrom.toPath().toString());
            return apkDest;
        }
    /**
     * Copy de most recent ebook to the destination given
     * Also deletes any epub file remanent.
     * @param ebook
     * @param destPath
     */
    private static File copyEpubToDestination(String ebook, String destPath) throws IOException {
        deleteFilesOnDestination(destPath);
        File epub = getLatestEpub(ebook);
        if (epub ==null) {
            System.out.println("Error: missing epub file on path:"  + ebook);
            return null;
        }
        System.out.println("Copying to destination:" + epub.getName());
        File destination = new File(destPath +  epub.getName());
        Files.copy(epub.toPath(),destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
        return destination;
    }

    /**
     * Given an ebook, find the latest epub files , otherwise return null
     * Reference: How do we get the most recent file? R: https://www.javacodex.com/More-Examples/1/1
     * @param ebook
     * @return
     */
    public static File getLatestEpub(String ebook) {
        String dirEpub = String.format(EBOOK_EPUB,ebook);
        File dir = new File(dirEpub);
        File[] files = dir.listFiles();
        Arrays.sort(files, new Comparator<File>(){
            public int compare(File f1, File f2) {
                return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
            }
        });
        File epub= null;
        for(File file: files) {
            if (file.getName().endsWith("epub")) {
                epub =file;
                break;
            }
        }
        return epub;
    }

    public static File getLatestApk(String ebook) {
        String dirEpub = String.format(EBOOK_APK,ebook);
        File dir = new File(dirEpub);
        File[] files = dir.listFiles();
        Arrays.sort(files, new Comparator<File>(){
            public int compare(File f1, File f2) {
                return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
            }
        });
        File epub= null;
        for(File file: files) {
            if (file.getName().endsWith("apk")) {
                epub =file;
                break;
            }
        }
        return epub;
    }

    /**
     *  Iterate over destination, delete all *.epub files
     * @param pathDestination
     */
    private static void deleteFilesOnDestination(String pathDestination) {
        File dirDestination = new File(pathDestination);
        File[] dirListing = dirDestination.listFiles();
        for (File child: dirListing) {
            if (child.getName().endsWith(".epub")) {
                System.out.println("Deleting from destination:" + child.getName());
                child.delete();
            }
        }
    }

    /**
     * 20180829: improved method. Executes command line, wait for output and results,dump to screen
     *
     * @throws IOException
     * @throws InterruptedException
     * @param directory
     * @param cmdLine
     * @param msg
     */
    public static void executeCommandLine(String directory, String cmdLine, String msg) throws IOException, InterruptedException {
        System.out.println(msg);
        ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", cmdLine);
        File dir = new File (directory);
        pb.directory(dir);
        Process process = pb.start();
        IOThreadHandler outputHandler = new IOThreadHandler(process.getInputStream());
        outputHandler.start();
        process.waitFor();
        System.out.println(outputHandler.getOutput());
   }

        /*
        Create the icons for the project.
        This process needs: 2 icons in the ebookxxx/graphics/icons  named icon-512x512.png and icon-512x512-circle.png
        A .bat file (created_icons.bat) where the process can call it globally.
        This method calls the bat file from the folder above detailed, creating a system of folders and icons to be
        copied to FolioReader Project.
         */
        public static void createIcons (String ebook) throws IOException, InterruptedException {
            //MAKE SURE THE ICONS ARE IN PLACE WITH THE PROPER NAMES!!!!
            String cmd = String.format(EBOOK_ICONS,ebook);
            ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c","create_icons.bat" );
            File dir = new File (cmd);
            pb.directory(dir);
            Process p =pb.start();
        }

    /**
     * Copy icons from generation folder to destination on FolioReader project
     * @param ebook
     * @throws IOException
     */
    public static void copyIcons(String ebook) throws IOException, InterruptedException {
            String from = String.format(EBOOK_ICONS,ebook);
            String dest = FR_EPUB_ICONS;
            File dirFrom = new File(from);
            File[] dirListing = dirFrom.listFiles();
            for (File child: dirListing) {
                if (child.isDirectory()) {
                    //Resolver con https://stackoverflow.com/questions/9370012/waiting-for-system-to-delete-file
                    DeleteDir(dest + child.getName());
                   // executeCommandLine(from,"rmdir " + dest + child.getName() +" /s","Deleting dir from destination..." );
                    System.out.println("Copying dir " + child.getName() + " to destination");
                    FileUtils.copyDirectory(child,new File(dest + child.getName()));
                    //This process needs some time to copy and process it, without it we have errors.
                    Thread.sleep(2000);
                }
            }
        }


        //"rmdir example /s

    /**
     * Delete a directory and and wait until is deleted
     * 20180915: I checked that this is NOT working, there is no reason for that.
     * @param DirToDelete
     * @throws IOException
     * @throws InterruptedException
     */
    public static void DeleteDir(String DirToDelete) throws IOException, InterruptedException {
        File ddd= new File(DirToDelete);
        if (ddd.exists()) {
            FileUtils.deleteDirectory(ddd);
            while (ddd.exists()) {
                Thread.sleep(100);
            }
        }
    }
    /**
     * Self describing...
     * @param ebook
     * @param key
     * @param keypwd
     * @throws IOException
     */
        public static void createKeystoreRepo(String ebook,String key, String keypwd) throws IOException, InterruptedException {
            //Create the directory if doesn't exist
            String cmd = String.format(ALEXANDRIA_ROOT + ebook);
            ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c","md keystore" ); //Create keystore if it doesn't exist
            File dir = new File (cmd);
            pb.directory(dir);
            Process p =pb.start();
            Thread.sleep(500); //Wait half a second before proceeding
            cmd = String.format((ALEXANDRIA_ROOT + ebook + "/keystore/"));
            pb = new ProcessBuilder("cmd.exe", "/c","addkey-alexandriabookseng " + key + " "  + keypwd ); //Create keystore if it doesn't exist
            dir = new File (cmd);
            pb.directory(dir);
            p =pb.start();
        }

    /**
     * Rewrite build.gradle and strings.xml using metadata in the current ebook
     * @param ebook
     * @throws IOException
     */
    public static void reconfigureProject(String ebook) throws IOException {

        Config c = new Config(ebook);
        executeTemplate(c, "build.gradle.mustache", FR_BUILD_GRADLE_FILE);
        executeTemplate(c, "strings.xml.mustache", FR_STRINGS_XML);
    }

    /**
     * Reconfigures some pages from the ebook and saves it into temp folder.
     * @param ebook
     * @throws IOException
     */
    public static void generateTitlePage(String ebook) throws Exception {

        Config c =  new Config(ebook);
        executeTemplate(c, "title.xhtml.mustache", TMP_FOLDER + EPUB_FOLDER + "Text/title.xhtml");
        XmlTools.addFileToContentOPF("Text","title.xhtml");
    }

    /**
     * using the ebook metadata configuration, writes the template in the destination specified.
     * Update: generalized method, passing an object.
     * @param c
     * @param template
     * @param fileDestination
     * @throws IOException
     */
    public static void executeTemplate(Object c, String template, String fileDestination) throws IOException {
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache m = mf.compile(template); //under resources folder(!)
        StringWriter writer = new StringWriter();
        m.execute(writer,c).flush(); //Execute template, return results in writer object
        BufferedWriter bwr = getWriter(fileDestination);         //Write contents to the specified file
        bwr.write(writer.toString());
        bwr.flush();
        bwr.close();
        System.out.println("updated:" + fileDestination);
    }


    /*
    Given a (HTML) document replace ONE ocurrence of text for another one.
     */
    public static void replaceTag (String filePath, String searchFor, String Replacement) {
        try {

            File f = new File(filePath);
            if (!f.isFile()){
                System.out.println("replaceTag ERROR: missing File " + f.getName() + "aborting...");
                return;
            }


            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
            StringBuffer sb = new StringBuffer();

            String line="";
            Pattern r = Pattern.compile(searchFor);
            Matcher m = r.matcher(line);
            Boolean flag = true;
            while ((line = br.readLine()) != null) {
                m = r.matcher(line); //Check if there is a perfect match . this method will work only once
                if (flag) {
                    if (m.find()) {
                        line=Replacement; //replace match with the line
                        flag=false; //do this just one time.
                    }
                }
                sb.append(line + "\n");
            }
            BufferedWriter bwr =getWriter(filePath);
            bwr.write(sb.toString());
            bwr.flush();
            bwr.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

/*
Removes all changes on all pages.
Warning: this is trying to solve the issue just once.
Parameters: json path of Table of Contents
default search string where we would insert audio (default is <body> but it is a bad idea putting audio below body
default search string to undo replacement.
20181210: Falto explicar que se requiere realizar DOS pasos, uno es resetear el tag body y el otro insertar el audio donde se indica.


 */
    public static void removeAudioEpub(String jsonTocPath, String defaultSearch, String defaultSearchUndo) throws IOException {

        //Get an empty template to reset changes
        //Very important trick: when removing the template, make sure than only checks beggining and end fo template
        //(because both (begin, end) does not change, however the middle can change).
        // Calling and empty template y used to erase the template pages.
        List<String> result =getStrings(""); //Get lines to be removed . IT DOES NOT MATTER WHAT CONTENT IS INSIDE. ONLY FIRST AND LAST LINE!!!!!
        File f = new File(jsonTocPath);
        if(!f.exists() ) {
            // do something
            System.out.println("removeAudioEpub ERROR: File toc.json missing ,can't continue!!!");
            return;
        }
        String pages =  new String(Files.readAllBytes(Paths.get(jsonTocPath)), StandardCharsets.UTF_8);
        JSONObject jsonPages = new JSONObject(pages);
        JSONArray pageArray = jsonPages.getJSONArray("chapter_list"); //Obtener array de capitulos
        String page="";
        for(int i=0; i<pageArray.length();i++) //Iterar por cada pagine (de momento cada capitulo es UNA pagina)
        {
            JSONObject pageData= pageArray.getJSONObject(i).getJSONObject("pagedata"); //obtener página
            page = pageData.getString("page");  //obtener camino página (ejemplo: Text\chapter2.xml)
            //insertAudioTag(TMP_FOLDER + EPUB_FOLDER +  page, defaultSearch,result,true);
            deleteAudioTag(TMP_FOLDER + EPUB_FOLDER +  page, defaultSearch,result);
            replaceTag(TMP_FOLDER + EPUB_FOLDER +  page, defaultSearch, defaultSearchUndo); //Resets body tag
        }
    }

    /*
    Dado la lista de paginas , la lista de mp3 y el template , realiza :
        -Para cada página:
            -Si no hay audio, pasa a la siguiente
            -Si hay audio:
                -extraer el link de audio  y combinarlo en el template
                -merge del template en la página
                -Guardar la página.
        Parece simple...
        20180819: Improved
        Se hacen DOS busqueda separadas:
        -Buscar donde se inserta el template de audio e insertarlo
        -Buscar donde se encuentra el tag BODY y reemplazarlo por template /script apropiado si aplica.
        20180824: mejorado: se busca el insert_audio_after dentro del archivo toc.json que fija una manera seteable de determinar donde insertar el audio.
        IMPORTANTE: la expresion regular tiene que ser exactamente igual como si se escribiera en java Por ejemplo <\\/H2> observar que la barra es doble como en java.
     */
    public static void addAudioEpub(String jsonTocPath, String jsonMp3Path, String regexpInsertAudio, String regexpBodyTagSearch) throws IOException {
        String mp3Url = null;
        File f = new File(jsonTocPath);
        if(!f.exists() ) {
            System.out.println("addAudioEpub ERROR: File toc.json missing ,can't continue!!!");
            return;
        }
         f = new File(jsonMp3Path);
        if(!f.exists() ) {
            System.out.println("addAudioEpub ERROR: File mp3.json missing ,can't continue!!!");
            return;
        }
        String pages =  new String(Files.readAllBytes(Paths.get(jsonTocPath)), StandardCharsets.UTF_8);
        String mp3    = new String(Files.readAllBytes(Paths.get(jsonMp3Path)), StandardCharsets.UTF_8);
        JSONObject jsonPages = new JSONObject(pages);

        JSONArray pageArray = jsonPages.getJSONArray("chapter_list"); //Obtener array de capitulos
        String regExpAudio = jsonPages.getString("insert_audio_after");
        if (regExpAudio !=null) {
            regexpInsertAudio = regExpAudio;
        }
        StringWriter writer = new StringWriter();
        int audioId = 0;
        int delay =0;
        String page = "";
        String bodyTag = BODY_TAG_SCRIPT_TEMPLATE;

        for(int i=0; i<pageArray.length();i++) //Iterar por cada pagina (de momento cada capitulo es UNA pagina)
        {
            JSONObject pageData= pageArray.getJSONObject(i).getJSONObject("pagedata"); //obtener página
            audioId = pageData.getInt("audio"); //Obtener audio
            delay = pageData.getInt("delay");
            page = pageData.getString("page");  //obtener camino página (ejemplo: Text\chapter2.xml)

            if (audioId == 0) continue; //Si el audio es cero, buscar la siguiente

            mp3Url = getAudioUrl(audioId,mp3); //Obtener la url del archivo mp3
            if (mp3Url != null) {
                List<String> result = getStrings(mp3Url); //Call mustache and build the special tags, return the list.
                //Insert tag list in the page.

                insertAudioTag(TMP_FOLDER + EPUB_FOLDER +  page, regexpInsertAudio,result);
                System.out.println("Added audio to " + page);
                if (delay!=0) {

                    replaceTag(TMP_FOLDER + EPUB_FOLDER +  page, regexpBodyTagSearch,String.format(bodyTag,delay));
                    System.out.println("Added " + delay + " seconds");
                }
            }
        }
    }

    /*
    Render the templates and return a list with the results.
     */
    public static List<String> getStrings(String mp3Url) throws IOException {
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache m = mf.compile("template.mustache"); //under resources folder(!)
        return getStrings(m,mp3Url);
    }

    public static List<String> getStrings(Mustache m, String mp3Url) throws IOException {
        StringWriter writer = new StringWriter();
        m.execute(writer,mp3Url).flush(); //Ejecutar template, devolver resultado en writer
        String lines[] = writer.toString().split("\\r?\\n");
        return Arrays.asList(lines); //Convierto resultado en lista
    }

    //Given audioId, returns mp3 url , return empty string if no matches
    public static String getAudioUrl(int audioId, String jsonData)
    {
        int mp3Id =-1;
        JSONObject obj = new JSONObject(jsonData);
        //Get the array
        JSONArray mp3List = obj.getJSONArray("mp3_list");
        //Get the first url, just for example.
        //Normally you would pass only the object to Mustache template and this has to handle it.
        for (int i=0; i<mp3List.length();i++)
        {
            mp3Id = mp3List.getJSONObject(i).getJSONObject("mp3").getInt("id");
            if (mp3Id ==audioId) {
                return mp3List.getJSONObject(i).getJSONObject("mp3").getString("url");
            }
        }
        return "";
    }

    /*
    insertAudioTag: appends text after some specific match (option remove=false)
              optionally removes text from file (option remove=true)
    IMPORTANT: this method overwrites the file in both cases.
    version 2.0: remove option (when remove=true) only checks Start Line and Last Line, in order of avoiding internal
    files who could change dinamically.
    Potential issues:
    remove mode: If file has start line but not end file, the result suppress all lines after start line
                 This behavior is for construction.
   20180829: El abrir los archivos codificados en utf-8 es increiblemente enrevesado. Fixed
   20181210: Faltó resolver el siguiente incidente, que generar muchos dolores de cabeza:
    textToaAppend contiene una lista de strings. De esta lista , SOLO IMPORTA LA PRIMERA Y ULTIMA LINEA, porque estas dos lineas determinan que borrar de la página.
Sin embargo tenemos un interesante desafío: Que sucede cuando en una página se agregan sucesivos tags?
Este problema que no se había contemplado antes, muestra que si se agregan varios tags en una misma página, estos quedan en orden inverso de ejecucion(!!!)
Por lo tanto, es preciso un workaround a este incidente.
Este proceso asume menos cosas que la versión anterior.
 */
    public static void insertAudioTag(String filePath, String searchFor, List<String> textToAppend) {
        try {
            File f = new File(filePath);
            if (!f.isFile()){
                System.out.println("insertAudioTag ERROR: missing File " + f.getName() + "aborting...");
                return;
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));

            StringBuffer sb = new StringBuffer();

            String line="";
            Pattern r = Pattern.compile(searchFor);
            Matcher m = r.matcher(line);

            Boolean taskFinished = false;

            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");

                //check if line match string searched for, and if it is, append it
                m = r.matcher(line);
                if (taskFinished) continue; //if the task was finished, avoid inserting the tag again.
                if (m.find())
                {
                    //Check if the next line starts with a tag . If it does, we need to skip it
                    //Notice that this can be repeated n times (!)
                    line = br.readLine();
                    while (line.startsWith(textToAppend.get(0))){
                        System.out.println("Found another tag, forwarding tag...");
                        sb.append(line + "\n");
                        String lastLine = textToAppend.get(textToAppend.size()-1);
                        while(!line.startsWith(lastLine)) { //While I cannot find the last line, skip the lines.
                            line = br.readLine();
                            sb.append(line + "\n");
                        }
                        //All the lines were skipped. this is the end of removing.
                        line = br.readLine();
                    }
                    //Now we can include the tag safely IN ORDER!
                    for(int i=0; i<textToAppend.size();i++){
                        sb.append(textToAppend.get(i) + "\n");
                    }


                    sb.append(line + "\n"); //Write the missing line.

                    taskFinished=true; //avoid inserting the tag again, but read the rest of the lines.
                }
            }

            //Write contents to the same file
            BufferedWriter bwr = getWriter(filePath);
            bwr.write(sb.toString());
            bwr.flush();
            bwr.close();

            //System.out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * New version: It searches for tag code and deletes ALL tags inside the page.
     * Documentation: the process looks for the following format:
     * <!--Custom data START-->
     * ...
     * <!--Custom data END-->
     *     and it deletes all the code inside those tags, including both tags.
     *
     *     This process also deletes ALL tags in the page. If there are two , three or n Custom tags, it will erase it all.
     *     This is a heavy simplification on the previous process, which tried to handled everything. It worked however it didn't take accout
     *     the case of multiple tags.
     * @param filePath
     * @param searchFor
     * @param textToAppend
     */
    public static void deleteAudioTag(String filePath, String searchFor, List<String> textToAppend) { //NOTE: REPLACE MERGEFILE MODE REMOVE
        try {
            File f = new File(filePath);
            if (!f.isFile()){
                System.out.println("insertAudioTag ERROR: missing File " + f.getName() + "aborting...");
                return;
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line="";
            Pattern r = Pattern.compile(searchFor);
            Matcher m = r.matcher(line);
            int foundTag=0;
            while ((line = br.readLine()) != null) {
                //Remove the lines
                if (line.startsWith(textToAppend.get(0))) {
                    foundTag++; //Let's count how many tags we found
                    //System.out.println("match in:" + line);
                    line=br.readLine(); //skip this line
                    //System.out.println("Match found in:" + filePath);
                    //Get the last line and start a loop for finding it
                    String lastLine = textToAppend.get(textToAppend.size()-1);
                    while(!line.startsWith(lastLine)) { //While I cannot find the last line, skip the lines.
                        line = br.readLine();
                    }
                    //All the lines were skipped. this is the end of removing.
                    line = br.readLine();
                }
                sb.append(line);
                sb.append("\n");
            }
            //if we didn't find, display a warning, since we never found where to put the data
            if (foundTag == 0) {
               // System.out.println("WARNING: place not found on page  "  + filePath);
            } else {
                System.out.println("FOUND: " +  foundTag + " tags in " + filePath);
            }
            //fr.close();
            //Write contents to the same file
            BufferedWriter bwr = getWriter(filePath);
            bwr.write(sb.toString());
            bwr.flush();
            bwr.close();

            //System.out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String  getFirstEpub(String dir)
    {
        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                if (file.getName().endsWith("epub")) {
                    return file.getName();
                }
            }
        }
    return "";
    }


    /**
     * Test 001: can unzip a file correctly?
     * Nope: review https://www.mkyong.com/java/how-to-decompress-files-from-a-zip-file/
     * @param zipFile
     * @param folder
     * @throws IOException
     */
    public static void unzip(File zipFile,String folder) throws IOException {

        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
        ZipEntry zipEntry = zis.getNextEntry();
        while(zipEntry != null){
            String fileName = zipEntry.getName();
            File newFile = new File(folder + "/" + fileName);
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }
    /*
     *Extract toc.ncx from an epub file
     * Unzip the files, and put it where?
     *Reference: https://www.baeldung.com/java-compress-and-uncompress
     * Needs to be improved: just ignore what epub name is, get the first one.
     * epubDir: dir where epub resides, it will try to take the first epub found.
     * fileToextract: file to extract, currently toc.ncx
     * saveFile: where to save the file, currently TMP folder with same name
     */
    public static void extractTOC(String epubDir, String fileToExtract,String saveFile) throws IOException {
        byte[] buffer = new byte[1024];
        String fileZip = getFirstEpub(epubDir);
        if (fileToExtract.length()==0) {
            System.out.println("ERROR: epub not found on " + epubDir);
            return;
        }
        ZipInputStream zis = new ZipInputStream(new FileInputStream(epubDir + "\\" +  fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while(zipEntry != null){
            String fileName = zipEntry.getName();
            //System.out.println("file:" + fileName);
            if (fileName.endsWith(fileToExtract)) {
                System.out.println("Found:" + fileToExtract);
                File newFile = new File( saveFile);
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
        System.out.println("Extraction of " + fileToExtract + " finished!");
    }

    /*
     *Convert a toc.ncx file in a json file to be combined later with audio files properly.
     *Entry: tocPath= toc.ncx file path
     *jsonPath = json file path
     * 20180824: Added option insert_audio_after, allowing the chance of custom configuration per epub.
     * 20180826:Improved: pages in Gutenberg could end with #page0010 (interal html markers)
     * We need to ride of the markers before adding as a page.
     * 20181118: Very strange having one error after all I did...
     * The error is happening because there is a tag named <text> outside of navMap
     * This is kind of weird: in 40 ebooks, I never saw that mistake(!?)
     * The solution is TO SUGGEST rebuild the index and that's it.
     */

    public static void tocJson(String tocPath,String jsonPath) throws FileNotFoundException,UnsupportedEncodingException,IOException
    {
        //Load file in one step
        String text = new String(Files.readAllBytes(Paths.get(tocPath)), StandardCharsets.UTF_8);

        String regexpTitle ="<text>(.*?)<";
        Pattern r = Pattern.compile(regexpTitle);
        Matcher m = r.matcher(text);
        List<String> titles = new ArrayList<String>();

        Boolean flag= true;
        while(m.find())
        {
            //System.out.println("find match:" + m.group(1) );
            if (flag) {
                flag=false;
                continue; //Ignore first description,since it is Document description
            }
            titles.add(m.group(1)); //add description to the list.
        }

        //Repeat process for pages
        regexpTitle ="<content(.*?)src=\"(.*?)\"";
        r= Pattern.compile(regexpTitle);
        m = r.matcher(text);
        List<String> pages = new ArrayList<String>();
        String[] htmlPage ;
        while(m.find())
        {
            //System.out.println("find match:" + m.group(2) );
            htmlPage = m.group(2).split("#");
            if (htmlPage.length>1)
            {
                pages.add(htmlPage[0]);
            } else
            {
                pages.add(m.group(2)); //add description to the list.
            }
        }

        //check if both list have same length:
        if (titles.size() != pages.size()) {
            System.out.println("ERROR!!!: description list has different size than page's list ("  + titles.size() + "," + pages.size() + ")");
            System.out.println("SUGGESTION: Use Sigil to correct or rebuild Table of Contents!!!");
            return;
        }

        int id=0;
        String line="";
        File tocJson = new File(jsonPath);
        if (tocJson.isFile()) {
            if (!YesNo("There is a toc.json file already!Overwrite(Y/N)",
                    "You decided NOT to overwrite, toc.json file process aborted",""))
            {
                return;
            }
            FileUtils.forceDelete(tocJson);             //Delete file and continue
        }

        Writer outputFile = getWriter(jsonPath);

        outputFile.write("{\n\"insert_audio_after\":\"" + REGEXP_INSERT_AUDIO + "\",");
        outputFile.write("\n\"chapter_list\":\n [\n");
        for(int i=0;i<titles.size();i++) {
            id++;
            line= String.format("{\"pagedata\":{\"id\":\"%s\",\"audio\":\"0\",\"delay\":\"0\",\"description\":\"%s\",\"page\":\"%s\"}}",id,titles.get(i),pages.get(i));

            if (i==(titles.size()-1))
            {
                outputFile.write(line);
            } else
            {
                outputFile.write(line +",\n");
            }
        }
        outputFile.write("\n]\n}");
        outputFile.close();
        System.out.println("FINISHED toc.json processing");
    }

    /**
     * Improved: BufferedWriter now explicitely define an encoding, which made many errors in the past...
     * @param filePath
     * @return
     * @throws UnsupportedEncodingException
     * @throws FileNotFoundException
     */
    public static BufferedWriter getWriter(String filePath) throws UnsupportedEncodingException, FileNotFoundException {
        return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"));
    }

    /*
     *Open a Librivox page, and generates JSON file containing all the mp3 files (64kbps) in it
     *the JSON file is used in combination with epub pages to include the audio.
     *NOTES: Maybe we need to add also an audio start for cases where file needs to be put forward.

     Version 2: the first version was too specific (for Moby Dick) , the new version is more generic.
     Version 3: added multiple url , separated by comma
     this is used specially with the book War and Peace Leon Tolstoy. Check ebook015
     Verion 4: Added chance of changing regular expression for capturing mp3 title just configuring metadata(!!!)
     */



    public static void extractMp3Url(String ebook,String[] urls, String pathJsonFile) throws Exception {

        String buffer="";
        if (urls.length==1) {
            buffer = getUrlPage(urls[0]); //the old usual way
        } else
        {
            buffer = getUrlPage(urls);   //Get multiple lines from multiples urls
        }

        String mp3Match= REGEXP_FIND_MP3_64KB;
        String mp3RegExpMetdata = Config.getMetadataParameter(ebook,"audio-regexp");
        String mp3TitleMatch = REGEXP_FIND_MP3_TITLE;
        //Give chance of using proper descriptions from metadata configuration instead hardcoded one!
        if (!mp3RegExpMetdata.equals(METADATA_NOT_FOUND)&& !mp3RegExpMetdata.equals(METADATA_EMPTY) ) {
            mp3TitleMatch= mp3RegExpMetdata;
        }

        Pattern r = Pattern.compile(mp3Match);
        Matcher mMp3 = r.matcher(buffer);

        r =Pattern.compile(mp3TitleMatch);
        Matcher mTitle =r.matcher(buffer);

        //Check if file is there
        File mp3File = new File(pathJsonFile);
        if (mp3File.isFile()) {
            if (!YesNo("There is a mp3.json file already!Overwrite(Y/N)",
                    "You decide NOT to overwrite, mp3 file process aborted","")){
                return;
            }
            //Clear de file and continue
            FileUtils.forceDelete(mp3File);
        }

        System.out.println("Creating mp3.json file...");
        Writer outputFile = getWriter(pathJsonFile);
        String line;

        outputFile.write("{\n\"mp3_list\":\n [\n");
        Boolean flag=true;
        int id=0;
        while(mMp3.find())
        {
            if (flag) {
                flag=false;
            }else
            {
                outputFile.write(",\n");
            }
            id++;
            if (mTitle.find())
            {
                line= String.format("{\"mp3\":{\"id\":\"%s\",\"description\":\"%s\",\"url\":\"%s\"}}",id,mTitle.group(3).trim(),mMp3.group(1).trim());
            } else
            {
                line= String.format("{\"mp3\":{\"id\":\"%s\",\"description\":\"%s\",\"url\":\"%s\"}}",id,"TITLE NOT FOUND",mMp3.group(1).trim());
            }

            outputFile.write(line);
        }
        outputFile.write("\n]\n}");
        outputFile.close();
        System.out.println("mp3.json file created");
    }

    public static String getUrlPage(String url) throws IOException {
        String buffer="";
        URL urlPage = new URL(url);
        BufferedReader in = new BufferedReader( new InputStreamReader(urlPage.openStream(),"UTF-8"));

        String inputLine;
        while ((inputLine = in.readLine()) != null){
            buffer+=inputLine;
        }
        in.close();
        return buffer;
    }

    public static String getUrlPage(String[] urls) throws IOException {
        String buffer="";
       // System.out.println("Multiple mp3 urls detected");
        for (int i=0;i<urls.length;i++){
            URL urlPage = new URL(urls[i]);
            System.out.println("Scanning " + urlPage + " ...");
            BufferedReader in = new BufferedReader( new InputStreamReader(urlPage.openStream(),"UTF-8"));

            String inputLine;
            while ((inputLine = in.readLine()) != null){
                buffer+=inputLine;
            }
            in.close();
        }

        return buffer;
    }


    //
    /*
    La validación del metadata tiene que obedecer a diferentes niveles
    Por ejemplo, no puedo indicar que los screenshots no están prontos cuando lo único que quiero es descompactar un epub.
    Por lo tanto defino los siguientes niveles que chequean diferentes niveles de cosas.


    GOOGLE_PLAY = que es lo necesario para que funcione
    -Archivo ASO generado (no necesariamente, recomendado),esto puede hacerse al hacer unpackage.
    -Cover tiene que existir para agregarse a los screenshots
    -Screenshots tienen que tener el tamaño adecuado y la cantidad adecuada
    -Imagenes 512x512 y 500x1024 tienen que estar presentes
    -La aplicación tiene que estar compilada y pronta.

    ALL = valida el conjunto total de validaciones, es para un chequeo genearal PREVIO a subir a Google Play

     */



    private static class IOThreadHandler extends Thread {
        private InputStream inputStream;
        private StringBuilder output = new StringBuilder();

        IOThreadHandler(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        public void run() {
            Scanner br = null;
            try {
                br = new Scanner(new InputStreamReader(inputStream));
                String line = null;
                while (br.hasNextLine()) {
                    line = br.nextLine();
                    output.append(line
                            + System.getProperty("line.separator"));
                }
            } finally {
                br.close();
            }
        }

        public StringBuilder getOutput() {
            return output;
        }
    }


    public static class imdbList {
        public String bookname ="<NONE>";
        public List<movie> movieList = new ArrayList<movie>() ;

        public void addMovie (String id, String title, String url, String urlImage){
            movieList.add(new movie(id,title,url,urlImage));
        }
        /**
         * Class used for retrieve data from IMDB site.
         */
            static class movie {
                public movie(String id, String title, String url, String urlImage) {
                    this.id =id;
                    this.title = title;
                    this.url = url;
                    this.urlImage =urlImage;
                }
                String id;
                String title;
                String url;
                String urlImage;
            }
    }

    public static class AsoList {

        public List<asoItem> asoList = new ArrayList<asoItem>() ;

        public void addASO (String id, String title){
            asoList.add(new asoItem(id,title));
        }
        /**
         * Class used for retrieve data from IMDB site.
         */
        static class asoItem {
            public asoItem(String id, String description) {
                this.id =id;
                this.description = description;
            }
            String id;
            String description;
        }
    }

    public static class EbookData {
        String ebook;
        String number;
        public String title;
        public   String author;
        public String authormeta;
        public String bookurl;

        public EbookData(String ebook){
            //Extract number
            this.ebook =ebook;
            if (ebook.length() == 3) {
                this.number=ebook;
            } else if (ebook.length() > 3) {
                this.number = ebook.substring(ebook.length() - 3);
            } else {
                // whatever is appropriate in this case
                System.out.println("ERROR: ebook name has less than 3 characters...!");
            }
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAuthormeta() {
            return authormeta;
        }

        public void setAuthormeta(String authormeta) {
            this.authormeta = authormeta;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getBookurl() {
            return bookurl;
        }

        public void setBookurl(String bookurl) {
            this.bookurl = bookurl;
        }
    }
}
