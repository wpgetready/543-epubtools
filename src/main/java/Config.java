import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 20180917: Extended Config class with more metadata and arrays variables.
 * The main reason is this class is used for template mustache generation. Therefore, we need all metadata as posible included.
 */
class Config {

    public static final int IMG_W = 320;

    Config(String ebook) throws IOException {

    this.getConfig(ebook);
    }



    /*
            Reads a parameter from ebook metadata.
            Lines starting with # will be ignored
            Parameters are defined as following
            parameterName1=Value
            parameterName2=value2
            casing is IGNORED and spaces are also ignored before or after equal sign
            var1 =3
            VAR1=4
            means exactly the same, we can search for var1,VaR1, or VAR1, vAr1
            In this case it will return 3 since it appears FIRST. However if we erase the first line it will return
            4 for the reasons explained.
            if the parameter is not found, it will return METADATA_NOT_FOUND
            if the parameter is found but empty it will return METADATA_EMPTY

            20180828: Improved with caution: I found that in ebook006 keystore was using \ instead / which is making an error.
            So, I dinamically changed when load the data. It could be a bad idea so be careful...
            20180914: IT IS a bad idea indeed. The issue here is we can add regular expression inside metadata because it could fail.
            It is safer to properly convert parameters if they need to be converted outside this function.
            I created getMetadataParameterURL to solve this particular case for extracting and converting url
            20181118: Unexpected bug: if we use reg expresions and we use several = the result is splitted in an array with several parts, which
            is NOT what I intended. Solution is to introduce 2 as limit
             */
        public static String getMetadataParameter (String ebook,String parameter) throws IOException {
          String metadataPath = EpubTools.ALEXANDRIA_ROOT + "/" + ebook + "/metadata.txt";

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(metadataPath), "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line ="";
            String[] paramData;

            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) continue;
                paramData = line.split("=",2);
                if (paramData.length==0) continue;
                if (paramData[0].toLowerCase().trim().equals(parameter.toLowerCase().trim())){
                    if (paramData.length ==1) return EpubTools.METADATA_EMPTY;
                    return paramData[1].trim();
                }
            }
           // fr.close();
            br.close();
          return EpubTools.METADATA_NOT_FOUND;
        }

    public static void writeMetadataParameter(String ebook, String parameterComplete) {
        //Get data from commandline. Split data using the FIRST equal sign, ignore others.
        String[] splitData = parameterComplete.split("=",2);
        String parameter ="";
        String value="";
        if (splitData.length==0) {
            System.out.println("Invalid parameter input, it needs to be in the form paramName=paramValue");
            return;
        }
        if (splitData.length==1) {
            System.out.println("Parameter value is empty,will be reset");
            parameter = splitData[0];
            value="";
        } else
        {
            parameter = splitData[0];
            value = splitData[1];
        }


        try {
            String metadataPath = EpubTools.ALEXANDRIA_ROOT + "/" + ebook + "/metadata.txt";
            File f = new File(metadataPath);
            if (!f.isFile()){
                System.out.println("ERROR: missing metadata for ebook " + ebook + ". Is this ebook exist?. Aborting...");
                return;
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(metadataPath), "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line="";
            String[] paramData;
            Boolean taskFinished = false;

            while ((line = br.readLine()) != null) {
                paramData = line.split("=",2); //Read parameter
                if (paramData.length==0) continue; //ignore empty lines
                if (paramData[0].toLowerCase().trim().equals(parameter.toLowerCase().trim())){
                    System.out.println("Current parameter value:" + line);
                    line= parameter + "=" + value;
                    taskFinished=true;
                }
                sb.append(line + "\n"); //Write the missing line.
            }
            //Check if we finished the task, otherwise, write the parameter in the end.
            if (!taskFinished){
                System.out.println("Parameter " + parameter + " missing , creating it on the end of the file");
                sb.append(parameter+ "="+ value + "\n");
            }

            //Write contents to the same file
            BufferedWriter bwr = getWriter(metadataPath);
            bwr.write(sb.toString());
            bwr.flush();
            bwr.close();
            System.out.println("Parameter replace/append completed");
            //System.out.println(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Improved: BufferedWriter now explicitely define an encoding, which made many errors in the past...
     * @param filePath
     * @return
     * @throws UnsupportedEncodingException
     * @throws FileNotFoundException
     */
    public static BufferedWriter getWriter(String filePath) throws UnsupportedEncodingException, FileNotFoundException {
        return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"));
    }



    /**
     * Particular case above for returning urls
     * @param ebook
     * @param parameter
     * @return
     * @throws IOException
     */
        public static String getMetadataParameterURL(String ebook, String parameter) throws IOException {
            return getMetadataParameter(ebook,parameter).replace('\\','/');
        }

    /**
     * Overloaded version which returns an array.
     * The main difference:
     * de data returned is delimited as follow:
     * !parameter
     * data
     * data
     * ...
     * !END
     * We can use # inside to comment it!
     * @param ebook
     * @param parameter
     * @return
     * @throws IOException
     */
    public static String[] getMetadataParameterArray (String ebook,String parameter) throws IOException {
        String metadataPath = EpubTools.ALEXANDRIA_ROOT + "/" + ebook + "/metadata.txt";

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(metadataPath), "UTF-8"));
        StringBuffer sb = new StringBuffer();
        String line ="";

        String[] paramData;
        boolean found = false;
        //Step 1: find the variable
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) continue;
            paramData = line.split("=");
            if (paramData.length==0) continue;
            if (paramData[0].toLowerCase().trim().equals("!" + parameter.toLowerCase().trim())){
                found=true;
                break;
            }
        }
        //Step 2:collect data, ignore lines starting with '#'
        List<String> data = new ArrayList<String>();
        if (!found) return null;
        while ((line = br.readLine()) != null)  {
            if (line.trim().toLowerCase().equals("!end")){
                break;
            }
            if (line.startsWith("#")) continue;
            data.add(line.trim());
        }

        br.close();
        return data.toArray(new String[data.size()]); //Convert to array.
    }

    //Mmmm..hice las cosas al pedo dos veces. Para que valido los datos si YA los tengo?
    void getConfig(String ebook) throws IOException {
        this.ebookname = getMetadataParameter(ebook,"ebook-name");
        this.keystorepath = EpubTools.ALEXANDRIA_ROOT + ebook + "/" +  getMetadataParameterURL(ebook,"keystore");
        this.keystorepwd = getMetadataParameter(ebook,"keystore-pwd");
        this.keyalias = getMetadataParameter(ebook,"key");
        this.keypassword = getMetadataParameter(ebook,"key-pwd");
        this.applicationid = getMetadataParameter(ebook,"applicationid");
        this.versioncode = getMetadataParameter(ebook,"versioncode");
        this.versionname = getMetadataParameter(ebook,"versionname");
        this.admobbanner = getMetadataParameter(ebook,"admob_banner");
        this.admobinterstitial = getMetadataParameter(ebook,"admob_interstitial");
        this.admobappid = getMetadataParameter(ebook,"admob_appid");
//Exceptional case of linking to Google Play which could be empty the very first time
        this.googleplaylink = getMetadataParameterURL(ebook,"google-play-link");
        if (this.googleplaylink== EpubTools.METADATA_EMPTY) {
            this.googleplaylink="";
        }

        this.ebookauthor = getMetadataParameterURL(ebook,"ebook-author");
        this.originaldate = getMetadataParameterURL(ebook,"ebook-original-date");
        this.categories= getMetadataParameterURL(ebook,"ebook-categories");
        this.audiolink = getMetadataParameterURL(ebook,"audio-link"); //Corregir a array
        this.gutenberglink = getMetadataParameterURL(ebook,"ebook-gutenberg-link");

        //default values, can be changed from the overload configuraction
        String al = getMetadataParameterURL(ebook,"audio-link-text");
        if (!(al.equals(EpubTools.METADATA_EMPTY) || al.equals(EpubTools.METADATA_NOT_FOUND))) {
            this.audiolinktext =al;
        }

        String gl =  getMetadataParameterURL(ebook,"gutenberg-link-text");
        if (!(gl.equals(EpubTools.METADATA_EMPTY) || gl.equals(EpubTools.METADATA_NOT_FOUND))) {
            this.gutenberglinktext =gl;
        }

        this.biography = getMetadataParameter(ebook,"ebook-biography");
        this.linksimdb = getMetadataParameterArray(ebook,"links-imdb");
        this.linksaso = getMetadataParameterArray(ebook,"links-aso");
        this.linksaudio = getMetadataParameterArray(ebook,"links-audio");
        this.linksaudiosingle ="#";
        if (this.linksaudio != null){
            if (this.linksaudio.length>0) {
                this.linksaudiosingle = this.linksaudio[0];
            }
        } else
        {
            System.out.println("WARNING: links-audio metadata missing");
        }

    }
    String ebookname;
    String keystorepath;
    String keystorepwd;
    String keyalias;
    String keypassword;
    String applicationid;
    String versioncode;
    String versionname;
    String admobbanner;
    String admobinterstitial;
    String admobappid;
    String googleplaylink;

    //New data added to create title.xhtml
    String biography;
    String originaldate;
    String categories;
    String audiolink;
    String gutenberglink;
    String audiolinktext="Librivox";
    String gutenberglinktext="Project Gutenberg";
    String ebookauthor;
    ///Array variables
    String[] linksimdb;
    String[] linksaso;
    String[] linksaudio; //WATCHOUT: Currently there is an audiolink and also an array audioLink. audiolink will be deprecated(!)
    String linksaudiosingle; //Used in templates, to get just the first link if exists.


    public static boolean isConfigurationValid(String ebook, EpubTools.validationLevel level) throws IOException, InterruptedException {
        boolean valid = true;
        System.out.println(""); //Separar results del comando

        switch (level) {
            case UNPACKAGE:
                valid = validateUnpackageOptions(ebook);
                break;
            case REPACKAGE:
                valid = validateRepackageOptions(ebook);
                break;
            case COMPILE:
                valid = validateCompileOptions(ebook);
                break;
            case GOOGLE_PLAY:
                valid = validateGooglePlayOptions(ebook);
                break;
            case ALL:
                valid = validateUnpackageOptions(ebook) && valid;
                valid = validateRepackageOptions(ebook) && valid;
                valid = validateCompileOptions(ebook) && valid;
                valid = validateGooglePlayOptions(ebook) && valid;
                break;
        }
        return valid;
    }
        /*
        System.out.println("\nChecking ebook " + ebook + "...");
        valid = validParameter(ebook, "ebook-name") && valid;
        valid = validParameter(ebook, "keystore") && valid;
        valid = validParameter(ebook, "keystore-pwd") && valid;
        valid = validParameter(ebook, "key") && valid;
        valid = validParameter(ebook, "key-pwd") && valid;
        valid = validParameter(ebook, "applicationId") && valid;
        valid = validParameter(ebook, "versionCode") && valid;
        valid = validParameter(ebook, "versionName") && valid;
        valid = validParameter(ebook, "admob_banner") && valid;
        valid = validParameter(ebook, "admob_interstitial") && valid;
        valid = validParameter(ebook, "admob_appId") && valid;
        valid = validParameterFound(ebook, "google-play-link") && valid;

        valid = isValidIcons(ebook);
        String path;
        File f;

        path = String.format(EpubTools.EBOOK_KEYSTORE,ebook);
        f= new File(path);
        if (!(f.exists() && !f.isDirectory())) {
            System.out.println("FAIL! missing Keystore file in " +  path);
            //Additional: a script making keystore using metadata provided. One by one...
            String key = getMetadataParameter(ebook,"key");
            String keyPwd = getMetadataParameter(ebook,"key-pwd");
            EpubTools.createKeystoreRepo(ebook,key,keyPwd);
            System.out.println("Keystore and key create. Re-run this program!!!");
            valid = false;
        } else
        {
            System.out.println("(PASS) Keystore file present");
        }

        if (!valid) {
            System.out.println("Result tests: FAIL! Please fill all the data before proceed!");
        }
        return valid;
        */

    /*
          This is a common of set of rules for validation metadata.txt files.
          if metadata is valid it will proceed.   All the explanations is dumped on console.
          Added new control for images 500x1024

          THIS SHOULD  BE OLD, but I need to extract correct data.

    public static boolean ebookMetadataValidation(String ebook) throws IOException, InterruptedException {

        boolean valid = true;
        System.out.println("\nChecking ebook " + ebook + "...");
        valid = validParameter(ebook, "ebook-name") && valid;
        valid = validParameter(ebook, "keystore") && valid;
        valid = validParameter(ebook, "keystore-pwd") && valid;
        valid = validParameter(ebook, "key") && valid;
        valid = validParameter(ebook, "key-pwd") && valid;
        valid = validParameter(ebook, "applicationId") && valid;
        valid = validParameter(ebook, "versionCode") && valid;
        valid = validParameter(ebook, "versionName") && valid;
        valid = validParameter(ebook, "admob_banner") && valid;
        valid = validParameter(ebook, "admob_interstitial") && valid;
        valid = validParameter(ebook, "admob_appId") && valid;
        valid = validParameterFound(ebook, "google-play-link") && valid;

        valid = isValidIcons(ebook) && valid ;
        valid = checkKeystoreFile(ebook);

        if (!valid) {
            System.out.println("Result tests: FAIL! Please fill all the data before proceed!");
        }
        return valid;
    }
*/
    /**
     * Checks keystore file, creates it if needed.
     * @param ebook
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public static boolean checkKeystoreFile(String ebook) throws IOException, InterruptedException {
        String path;
        File f;

        path = String.format(EpubTools.EBOOK_KEYSTORE,ebook);
        f= new File(path);
        if (!(f.exists() && !f.isDirectory())) {
            System.out.println("FAIL! missing Keystore file in " +  path);
            //Additional: a script making keystore using metadata provided. One by one...
            String key = getMetadataParameter(ebook,"key");
            String keyPwd = getMetadataParameter(ebook,"key-pwd");
            EpubTools.createKeystoreRepo(ebook,key,keyPwd);
            System.out.println("Keystore and key create. Re-run this program!!!");
            return false;
        } else
        {
            System.out.println("(PASS) Keystore file present");
            return true;
        }

    }

    /*
  UNPACKAGE = Voy a chequear que es necesario para el proceso de descompactación el cual detallo aquí:
    -La carpeta ebook000/OEPS (EBOOK_CUSTOM_PAGE) debe estar presente y existir si queremos copiar archivos desde ahi
    (y las carpetas Text e Images tienen que estar presente)
    -La variable audio-link (que AHORA ES UN ARRAY), debe existir al menos un link (aconsejable)
    -ebook-biography debe figurar, no es obligatorio pero si aconsejable.
    Si existe, tiene que existir las carpetas respectivas.
    -getLatestEpub(ebook) debe devolver al menos un epub.
    Luego, para generar title.xhtml precisamos:
    -ebook-name =titulo
    -ebook-author = Nombre del autor , esta NO es la carpeta
    -ebook-original-date
    -ebook-categories
    -ebook-gutenberg-link
    -ebook-gutenberg-link-text (opcional)
    -audio-link  (esto sería un array, alcanza el primer link)
    -audio-link-text (opcional)
    Para generar imdb.xhtml necesitamos:
    ebook-name (ya lo teniamos en title.xhtml
    -links-imdb (array)
 */
    private static boolean validateUnpackageOptions(String ebook ) throws IOException {
        boolean valid;
        File customFolder = new File(EpubTools.EBOOK_CUSTOM_PAGE);
        if (!customFolder.isDirectory() ){
            System.out.println( "(NOT PASS) missing folder: " + customFolder.toPath().toString());
            valid = false;
        } else {
            System.out.println( "(PASS) folder present: " + customFolder.toPath().toString());
            valid = true;
        }

        valid = validParameterFound(ebook,"ebook-biography") && valid;
        valid = validParameter(ebook,"ebook-name") && valid;
        valid = validParameter(ebook,"ebook-author") && valid;
        valid = validParameter(ebook,"ebook-original-date") && valid;
        valid = validParameter(ebook,"ebook-categories") && valid;
        valid = validParameter(ebook,"ebook-gutenberg-link") && valid;
        //valid = validParameter(ebook,"ebook-gutenberg-link-text") && valid;
        valid = validParameter(ebook,"links-audio",true) && valid;
        //valid = validParameter(ebook,"audio-link-text") && valid;
        valid = validParameter(ebook,"links-imdb",true) && valid;

        return valid;
    }
/*
    REPACKAGE = Chequeo que es necesario para generar un epub.
            -ebook-name
    -el folder del ebook debe existir
*/
    private static boolean validateRepackageOptions(String ebook) throws IOException {
        boolean valid=true;
        valid= validParameter(ebook,"ebook-name") && valid;
        File ebookFolder = new File(String.format(EpubTools.EBOOK_EPUB,ebook));
        if (ebookFolder.isDirectory()) {
            System.out.println("(PASS) ebook folder valid " + ebookFolder.toPath().toString());
            valid = true && valid;
        } else
        {
            System.out.println("(NOT PASS) missing ebook folder " + ebookFolder.toPath().toString());
            valid = false && valid;
        }
        return valid;
    }

    /*
        -COMPILE = Chequeo de lo necesario para compilar

    -Debe existir un epub
    -los iconos para redimensionar necesitan estar en el lugar y ser válidos
    (ya se está haciendo acutalmente el chequeo en data validation)
    Para el archivo gradle:
    OK-key
    OK-key-pwd
    OK-keystore (path)
    OK-keystore-pwd
    OK-applicationId
    OK-versionCode
    OK-versionName
    OK-admob_banner
    OK-admob_interstitial
    OK-admob_appId
     */

    private static boolean validateCompileOptions(String ebook) throws IOException, InterruptedException {
        boolean valid = true;
        valid = validParameterFound(ebook,"admob_banner") && valid;
        valid = validParameterFound(ebook,"admob_interstitial") && valid;
        valid = validParameterFound(ebook,"admob_appId") && valid;
        valid = validParameterFound(ebook,"keystore") && valid;
        valid = validParameterFound(ebook,"keystore-pwd") && valid;
        valid = validParameterFound(ebook,"key") && valid;
        valid = validParameterFound(ebook,"applicationId") && valid;
        valid = validParameterFound(ebook,"versionCode") && valid;
        valid = validParameterFound(ebook,"versionName") && valid;
        //check keystore and builds one if needed. In that case we need to re-run the options.
        valid = checkKeystoreFile(ebook) & valid;
        File latestEpub =EpubTools.getLatestEpub(ebook);
        if (latestEpub ==null) {
            System.out.println("(NOT PASS) missing epub in  " + ebook + " folder.");
            valid =false;
        } else
        {
            System.out.println("(PASS) valid epub found  " + latestEpub.toPath().toString());
            valid =true;
        }
//Falta algo: chequear que los iconos y carpetas estén en los lugares correctos.
        return valid;
    }

    /*
    GOOGLE_PLAY = que es lo necesario para que funcione
    -Archivo ASO generado (no necesariamente, recomendado),esto puede hacerse al hacer unpackage.
    OK  Se estandariza a cover.jpg o cover.png -Cover tiene que existir para agregarse a los screenshots
    OK -La carpeta Screenshots tiene que est presente
    OK -Screenshots tienen que tener el tamaño adecuado y la cantidad adecuada
   OK  -Debería de haber un mínimo de 4 screenshots (5 con el cover).
    OK-Imagenes 512x512 y 500x1024 tienen que estar presentes
PENDIENTE    -La aplicación tiene que estar compilada y pronta.

    ALL = valida el conjunto total de validaciones, es para un chequeo genearal PREVIO a subir a Google Play
     */
    private static boolean validateGooglePlayOptions (String ebook) throws IOException {
        boolean valid=true;
        boolean cover= true;
        valid = validParameter(ebook,"links-aso",true) && valid;
        String asoPage =String.format(EpubTools.EBOOK_OTHER,ebook) + "imdb/page/ASO.txt";
        boolean asoPresent = isValidFile(ebook,asoPage, false);
        if (!asoPresent) {
            System.out.println("(WARNING): ASO.txt missing:  " + asoPage);
        } else
        {
            System.out.println("(PASS): ASO.txt present:  " + asoPage);
        }
        valid = isValidIcons(ebook) && valid; //Check 512x512 and 500x1024 files
        valid = isValidDirectory(String.format(EpubTools.EBOOK_SCREENSHOTS,ebook)) && valid;
        valid = isValidScreenshots(String.format(EpubTools.EBOOK_SCREENSHOTS,ebook)) && valid;
        valid = isValidDirectory(String.format(EpubTools.EBOOK_COVER,ebook)) && valid;
        cover  = isValidFile(String.format(EpubTools.EBOOK_COVER,ebook),"cover.jpg") ;
        if (!cover) {
           cover= isValidFile(String.format(EpubTools.EBOOK_COVER,ebook),"cover.png") ;
        }
        valid = cover && valid;
        return valid;
    }

    /*
    Validate all images in folder:
    Check if dimensions are apropiate:
    Appropiate means:
        1-image width>320px
        2-image height>(image width)*1.5
     */
    private static boolean isValidScreenshots(String d) throws IOException {
        File dir = new File(d);
        File[] files = dir.listFiles();
        float relation=0f;
        boolean valid= true;
        int counter = 0;
        for (File f:files) {
            if (!f.getName().endsWith("jpg")) continue;
            if (!f.getName().endsWith("png")) continue;
            if (!f.getName().endsWith("gif")) continue; //gif? well...
            counter++;
            Dimension dim =ImageTools.getImageDimension(f);
            if (dim.width< IMG_W) {
                System.out.println( "(NOT PASS) screenshot " + f.getName() +  " width less than " + IMG_W + ". Make it wider!");
                valid=false;
            } else
            {
                System.out.println( "(PASS) screenshot " + f.getName() +  " aceptable width (" + dim.width);
            }
            //Check relation should be 1.5 or higher
            relation = dim.height/dim.width;
            if (relation<1.5) {
                System.out.println( "(NOT PASS) screenshot " + f.getName() +  " relation size  less than1.5 ( " +
                        dim.height  + "/" + dim.width + "=" + relation );
                valid=false;
            } else {
                System.out.println( "(PASS) screenshot " + f.getName() +  " aceptable relation  (" + relation + ")");
            }
        }
        if (counter<4) {
            System.out.println( "(WARNING) too few screenshots (" + counter + ")");
        } else {
            System.out.println( "(PASS) There are enough screenshots ("+ counter +")");
        }
        return valid;
    }
    private static boolean isValidDirectory(String dir) {
        File d = new File(dir);
        if (d.isDirectory()) {
            System.out.println( "(PASS) directory present =" + dir);
            return true;
        } else
        {
            System.out.println( "(NOT PASS) missing directory =" + dir);
            return false;
        }
    }

    private static boolean isValidFile (String dir, String file) {
        return  isValidFile(dir,file,true);
    }

    private static boolean isValidFile(String dir, String file, boolean showMsg) {
        File d = new File(dir + file);
        if (d.isFile()) {
            if (showMsg) {
                System.out.println( "(PASS) file present =" + file);
            }

            return true;
        } else
        {
            if (showMsg)  {
                System.out.println( "(NOT PASS) missing file =" + file);
            }
            return false;
        }
    }
    /**
     * Default option for validParameter
     * @param ebook
     * @param parameter
     * @return
     */

   public static boolean validParameter (String ebook, String parameter) {
        return validParameter(ebook,parameter,false);
   }
    /**
     * Checks parameter validation against metadata text file.
     * @param ebook
     * @param parameter
     * @return
     */
    public static boolean validParameter (String ebook, String parameter, boolean isArray) {

        try {
            if (!isArray) {
                String temp = getMetadataParameter(ebook,parameter);
                if (temp == EpubTools.METADATA_EMPTY)
                {
                    System.out.println("(NOT PASS) parameter " + parameter + " empty");
                    return false;
                }
                if (temp == EpubTools.METADATA_NOT_FOUND)
                {
                    System.out.println("(NOT PASS) parameter " + parameter + " not found");
                    return false;
                }
                System.out.println( "(PASS) " + parameter + "= " + temp );
            } else
            {
                String temp[] = getMetadataParameterArray(ebook,parameter);
                if (temp == null)
                {
                    System.out.println("(NOT PASS) parameter  " + parameter + " missing");
                    return false;
                }
                if (temp.length==0)
                {
                    System.out.println("(NOT PASS) parameter " + parameter + " has no data");
                    return false;
                }
                System.out.println( "(PASS) " + parameter + "= (" + temp.length + " elements)" );
            }

            return true;
        } catch (IOException e) {
            System.out.println("Error=" + e.getMessage());
            return false;
        }
    }

    /**
     * Very special case: google play link can be empty but can't be not found.
     * This is because google play link is made AFTER apk is published, and this happens ONLY after it.
     * So it could be possible the VERY FIRST version doesn't have a link because I cant' have the url!
     * @param ebook
     * @param parameter
     * @return
     */
    public static boolean validParameterFound(String ebook, String parameter) {

        try {
            String temp = getMetadataParameter(ebook,parameter);
            if (temp == EpubTools.METADATA_NOT_FOUND)
            {
                System.out.println("(NOT PASS) parameter " + parameter + " not found");
                return false;
            }
            if (temp == EpubTools.METADATA_EMPTY)
            {
                System.out.println("(NOT PASS) parameter " + parameter + " empty");

                return false;
            }
            System.out.println( "(PASS) " + parameter + "= " + temp );

            return true;
        } catch (IOException e) {
            System.out.println("Error=" + e.getMessage());
            return false;
        }
    }

    /**
     * Validate if icons are present and are the correct sizes.
     * @param ebook
     * @return
     * @throws IOException
     */
    public static boolean isValidIcons(String ebook) throws IOException {
        boolean valid;
        String icon1 = "icon-512x512.png";
        String icon2 = "icon-512x512-circle.png";
        String icon3 = "icon-500x1024.png";
        String icon3_alternative = "icon-500x1024.jpg";

        String path = String.format(EpubTools.EBOOK_ICONS, ebook);
        //Some files are expected in some places (icons)
        File f = new File(path + icon1);
        if (!(f.exists() && !f.isDirectory())) {
            System.out.println("(NOT PASS) icon " + icon1 + " missing in " + path);
            valid = false;
        } else {
            System.out.println("(PASS) icon " + icon1 + " present");
        }
        valid = ImageTools.checkImgSize(f, 512, 512);

        f = new File(path + icon2);
        if (!(f.exists() && !f.isDirectory())) {
            System.out.println("(NOT PASS) icon " + icon2 + " missing in " + path);
            valid = false;
        } else {
            System.out.println("(PASS) icon " + icon2 + " present");
        }
        valid = ImageTools.checkImgSize(f, 512, 512);

        f = new File(path + icon3);
        if (!(f.exists() && !f.isDirectory())) {
            f = new File(path + icon3_alternative);
            if (!(f.exists() && !f.isDirectory())) {
                System.out.println("(NOT PASS) icon " + icon3 + " or " + icon3_alternative + " missing in " + path);
                valid = false;
            } else {
                System.out.println("(PASS) icon " + icon3_alternative + " present");
                valid = ImageTools.checkImgSize(f, 1024, 500);
            }
        } else {
            System.out.println("(PASS) icon " + icon3 + " present");
            valid = ImageTools.checkImgSize(f, 1024, 500);
        }
        return valid;
    }

}
