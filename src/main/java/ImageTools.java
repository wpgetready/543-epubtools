import com.sun.org.apache.xpath.internal.operations.And;
import org.apache.commons.io.FilenameUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

/*
 * @author mkyong
 * Reference:  https://www.codejava.net/java-se/graphics/how-to-resize-images-in-java
 * //https://stackoverflow.com/questions/672916/how-to-get-image-height-and-width-using-java
 */
public class ImageTools {

    /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     * @param inputImagePath Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param scaledWidth absolute width in pixels
     * @param scaledHeight absolute height in pixels
     * @throws IOException
     */
    public static void resize(String inputImagePath , int scaledWidth, int scaledHeight, String outputImagePath)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    /**
     * Resize an image to the dimensions requested
     * Problem: for some reason, this method also crops the image, somehting I never asked for.
     * Update: I find that IMDB currently cut the image.
     * @param inputFile
     * @param scaledWidth
     * @param scaledHeight
     * @param outputImagePath
     * @throws IOException
     */
    public static void resize(File inputFile , int scaledWidth, int scaledHeight, File outputImagePath)
            throws IOException {
        // reads input image
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String extension =FilenameUtils.getExtension(outputImagePath.toPath().toString());

        // writes to output file
        ImageIO.write(outputImage, extension, outputImagePath);
    }

    /**
     * This version also works ok, however I want to depende less from Imagemagic
     * @param inputFile
     * @param scaledWidth
     * @param scaledHeight
     * @param outputImagePath
     * @throws IOException
     * @throws InterruptedException
     */
    public static void resizeImageick(File inputFile , int scaledWidth, int scaledHeight, File outputImagePath) throws IOException, InterruptedException {
        String c =String.format("magick %s -resize %sx%s! %s" , inputFile.toPath().toString(),scaledWidth,scaledHeight,outputImagePath.toPath().toString());
        System.out.println(c);
        EpubTools.executeCommandLine(EpubTools.ALEXANDRIA_ROOT,c, "Resizing image to " + outputImagePath.getName() + "...");
    }

    /**y
     *
     * Resizes an image by a percentage of original size (proportional).
     * @param inputImagePath Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param percent a double number specifies percentage of the output image
     * over the input image.
     * @throws IOException
     */
    public static void resize(String inputImagePath,
                              String outputImagePath, double percent) throws IOException, InterruptedException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        resize(inputFile,  scaledWidth, scaledHeight, new File(outputImagePath));
    }

    public static Dimension getImageDimension(File imgFile) throws IOException {
        int pos = imgFile.getName().lastIndexOf(".");
        if (pos == -1)
            throw new IOException("No extension for file: " + imgFile.getAbsolutePath());
        String suffix = imgFile.getName().substring(pos + 1);
        Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
        while(iter.hasNext()) {
            ImageReader reader = iter.next();
            try {
                ImageInputStream stream = new FileImageInputStream(imgFile);
                reader.setInput(stream);
                int width = reader.getWidth(reader.getMinIndex());
                int height = reader.getHeight(reader.getMinIndex());
                return new Dimension(width, height);
            } catch (IOException e) {
                System.out.println("Error reading: " + imgFile.getAbsolutePath());
            } finally {
                reader.dispose();
            }
        }

        throw new IOException("Not a known image file: " + imgFile.getAbsolutePath());
    }

//Download an image from an specific url.
    public static void downloadUrlImage(String urlImage, String filePath) {
        try {
            URL url = new URL(urlImage);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1!=(n=in.read(buf)))
            {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(response);
            fos.close();

        }catch  (MalformedURLException e)                 {
            System.out.println("ERROR incorrect url format:" + e.getMessage());
        }
        catch (FileNotFoundException e) {
            System.out.println("ERROR File not found:" + e.getMessage());
        }
        catch (IOException e) {
            System.out.println("ERROR I/O:" + e.getMessage());
        }

    }

    /**
     * Gets image dimensions for given file
     * @param imgFile image file
     * @return dimensions of image
     * @throws IOException if the file is not a known image
     */
    public static boolean checkImgSize(File imgFile, int expectedW, int expectedH) throws IOException {
        boolean valid=true;
        int pos = imgFile.getName().lastIndexOf(".");
        if (pos == -1)
            throw new IOException("(FAIL) No extension for file: " + imgFile.getAbsolutePath());
        String suffix = imgFile.getName().substring(pos + 1);
        Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
        while(iter.hasNext()) {
            ImageReader reader = iter.next();
            try {
                ImageInputStream stream = new FileImageInputStream(imgFile);
                reader.setInput(stream);
                int width = reader.getWidth(reader.getMinIndex());
                int height = reader.getHeight(reader.getMinIndex());
                if (width != expectedW) {
                    System.out.println("(FAIL) image "+ imgFile.getName() + " width is " + width + " and it is expected " + expectedW);
                    valid = false;
                }
                if (height != expectedH) {
                    System.out.println("(FAIL) image " +  imgFile.getName() + " height is " + height + " and it is expected " + expectedH);
                    valid = false;
                }
                if (!valid) return valid;
                System.out.println("(PASS) image " + imgFile.getName() + " size match: " +expectedW + "x" + expectedH);
                return valid;
            } catch (IOException e) {
                System.out.println("(FAIL) Error reading: " + imgFile.getAbsolutePath());
                return false;
            } finally {
                reader.dispose();
            }
        }

        //throw new IOException("Not a known image file: " + imgFile.getAbsolutePath());
        System.out.println("(FAIL) Not a known image file: " + imgFile.getAbsolutePath());
        return false;
    }
}