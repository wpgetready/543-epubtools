import java.util.ArrayList;

//This is a basic structure for filling with author data. It will be used to fill biography information.
public class AuthorData {
    public int id; //GoodReads Author Id
    public String name;
    public String biography; //in HTML format
    public String bornDate;
    public String deathDate;
    public String urlImage;
    public String finalImage; //This will be initially empty, then image is downloaded and properly transformed and finally assigned.
    public ArrayList<BookData> books = new ArrayList<BookData>();
}